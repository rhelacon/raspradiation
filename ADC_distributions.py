#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 18:57:44 2021

@author: rahch
"""
import numpy as np
#script to analyze ADC distributions
###############################################
z=0
nfiles=500
width = 2592
height = 1944
################################################
############to modify to change the directory of analysis##########

pname="/home/rahch/Workspace/Raspcam/DATA/Data_simu_Agosto_5_2021/"
#pname = "../../DATA/Data_Septiembre_2019/" #parent directory
#dirname = "data_beta_CDS_1000"
dirname="test_11_crosstalking_M2"
data_bool = False
bgname = dirname
pbgname="Results/ResultsSep/"
###################################################################
filenamef = "Y_CDS_" +"z" +str(z).zfill(3)+ "_f" #filename format
adc_histo = np.zeros(256, dtype=np.int)
if data_bool:
    MeanFrame = np.load(pbgname+bgname+"_Mean.npy")
    SigmaFrame = np.load(pbgname+bgname+"_sigma.npy")
else:
    MeanFrame = np.zeros((height,width))
    SigmaFrame = np.zeros((height,width))
for n in range(nfiles):
        #red_channel = np.zeros((height,width),dtype=np.uint8)
        #green_channel = np.zeros((height,width),dtype=np.uint8)
        tmpname = pname + dirname + "/" + filenamef + str(n).zfill(3) + ".npz"
        tmp_data = np.load(tmpname)
        data = np.float64(tmp_data.f.arr_0)
        print(tmpname)
        if data_bool:
            threshold = abs(MeanFrame) +15.0*SigmaFrame #filter threshold
            tmp_frame = np.ones((height, width),dtype=np.float64)
        #ev_matrix = abs(data) > MaxFrame
            ev_matrix = abs(data) > threshold #filtering
            data_abs = np.trunc(abs(data)*ev_matrix)
        else:
            data_abs = np.abs(data)
        hist, bin_edges = np.histogram(data_abs, bins=np.arange(257), density=False)
        adc_histo += hist
        
np.savez_compressed("../"+dirname+"adc_distribution"+"Filter_"+str(data_bool)+"15sigma"+".npz", adc_histo)