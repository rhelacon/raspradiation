#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 13:56:30 2019

@author: rahch
"""
import numpy as np
import matplotlib.pyplot as plt
# Image size
width = 2592
height = 1944

filenamef = "RGB_noise_CDS_elect_hole_pairs_3.6eV_4_4300_amp1_z" #filename format
pname = "../../DATA/Data_Octubre_2021/" #parent directory
dirname = "data_Cs137_thickZ_2um_elect_hole_pairs_3.6eV_4_4300_amp1_ag8_noise_real_CDS" #Nombre directorio de la data
z_count = 1 # number of Z (Z varies 2mm for the data of September 2019, 1 mm otherwise)
hit_count =0 # #number of possible hits
nfiles = 50#number of files
fnfiles = np.float64(nfiles)
MeanFrame = np.zeros((height, width),dtype=np.float64)
SigmaFrame = np.zeros((height, width),dtype=np.float64)
SumFrame = np.zeros((height, width),dtype=np.float64)
Sum2Frame = np.zeros((height, width),dtype=np.float64)
#OldFrame = np.zeros((height, width),dtype=np.float64)
MaxFrame = np.zeros((height, width),dtype=np.float64)
for z in range(z_count):
    for n in range(nfiles):
        tmpname = pname + dirname + "/" + filenamef +str(z).zfill(3)+ "_f"+ str(n).zfill(3) + ".npz"
        tmp_nfiles = nfiles
        tmp_fnfiles = np.float64(nfiles)
        try:
            tmp_data = np.load(tmpname)
            ReadOk= True     
        except: 
            print("error on: " + tmpname)
            ReadOk= False
            tmp_nfiles= tmp_nfiles -1
            tmp_fnfiles= np.float64(tmp_nfiles)
        if(ReadOk):        
            data = np.float64(tmp_data.f.arr_0)
            SumFrame += data
            Sum2Frame += data**2
            print(tmpname)

            bo = abs(data)>=MaxFrame
            b1= abs(data)<MaxFrame
            MaxFrame = bo*abs(data) + b1*MaxFrame   
    #print(data.shape)    
 #   for i in range(height):
 #       for j in range(width):
 #           if(abs(data[i][j]) > MaxFrame[i][j]):
 #               MaxFrame[i][j]= abs(data[i][j])                
           

tmp_fnfiles = z_count*tmp_fnfiles    
MeanFrame = SumFrame/tmp_fnfiles
SigmaFrame = (Sum2Frame - tmp_fnfiles*(MeanFrame**2))/(tmp_fnfiles - 1.0)
SigmaFrame = np.sqrt(SigmaFrame)
np.save("Results/ResultsOct/"+dirname+"_sigma", SigmaFrame)
np.save("Results/ResultsOct/"+dirname+"_Mean", MeanFrame)
np.save("Results/ResultsOct/"+dirname+"_Max", MaxFrame)
