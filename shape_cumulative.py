#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 00:41:17 2020

@author: rahch
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 14:56:22 2020

@author: rahch
"""

#import matplotlib
#matplotlib.use('Qt5Agg')
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.stats import norm
#plt.figure(figsize=(38.4,21.6))


for n in range(10):
    plt.figure(figsize=(19.2,10.8))
    plt.rcParams.update({'font.size': 15})
    ax = plt.subplot(111)
    z = str(n)
    data_alpha0 = np.load("../data_alfa_CDS_1000/ShapeHistodata_alfa_CDS_1000z_"+z+"allclusters.npy")
    data_beta0 = np.load("../data_beta_CDS_1000/ShapeHistodata_beta_CDS_1000z_"+z+"allclusters.npy")
    data_gamma0 = np.load("../data_gamma_CDS_1000/ShapeHistodata_gamma_CDS_1000z_"+z+"allclusters.npy")
    data_rx0 = np.load("../data_RX_CDS_1s_27_11_19/ShapeHistodata_RX_CDS_1s_27_11_19z_"+z+"allclusters.npy")
    i = np.max(data_gamma0)
    j = np.max(data_beta0)
    k = np.max(data_alpha0)
    l = np.max(data_rx0)
    im = np.min(data_gamma0)
    jm = np.min(data_beta0)
    km = np.min(data_alpha0)
    lm = np.min(data_rx0)
#sizesimsr90 = data_simsr90.size 
    nmax = np.max([i,j,k,l])
    nmin = np.min([im, jm, km, lm])
    nbins = 200                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
#data_frame = np.vstack((data_beta0, data_gamma0, data_alpha0, data_rx0))
    lbls = ['Sr90', 'Cs137', 'Am241', 'Fe55']
    sns.set_palette("pastel")
    cond1= data_beta0 > 0
    cond2= data_gamma0 > 0
    cond3= data_alpha0 > 0
    cond4 = data_rx0 >0 
#plt.hist(data_beta0, bins=nbins,range=(nmin , nmax), density=True, histtype='step' , label='Sr90' , log=True,  linewidth=2)
    sns.histplot(np.extract(cond1,data_beta0),bins=nbins,stat="density",binrange=(-3.0 , 10), log_scale=(True, True), legend=True, label='Sr90', color='C0', element='step', fill=False, alpha=0.25, common_norm=False, linewidth=2,multiple="fill", cumulative=True)
    sns.histplot(np.extract(cond2,data_gamma0),bins=nbins,stat="density",binrange=(-3.0 , 10), log_scale=(True, True), legend=True, label='CS137', color='C1', element='step', fill=False, alpha=0.25, common_norm=False,linewidth=2, multiple="fill",cumulative=True)
#plt.hist(data_gamma0, bins=nbins,range=(nmin , nmax), density=True, histtype='step' , label='Cs137' , log=True, linewidth=2)
    sns.histplot(np.extract(cond3,data_alpha0),bins=nbins,stat="density",binrange=(-3.0 , 10), log_scale=(True, True), legend=True, label='Am241', color='C2', element='step', fill=False,  alpha=0.25, common_norm=False,linewidth=2,multiple="fill",cumulative=True)
#plt.hist(data_alpha0, bins=nbins, range=(nmin , nmax) , density=True, histtype='step' , label='Am241' , log=True, linewidth=2)
    sns.histplot(np.extract(cond4,data_rx0),bins=nbins,stat="density",binrange=(-3.0 , 10), log_scale=(True, True), legend=True, label='Fe55', color='C4', element='step', fill=False,  alpha=0.25, common_norm=False,linewidth=2, multiple="fill",cumulative=True)
#plt.hist(data_rx0, bins=nbins, range=(nmin , nmax) , density=True, histtype='step' , label='Fe55' , log=True, linewidth=2)
#fig.suptitle('Shapes Z= 0mm All Clusters')
    plt.xlim(-2.0, 10.0)
    plt.title("Shapes Z: "+ str(n*2) + "mm")
    plt.legend(loc="upper right")
    plt.savefig("../plots/Shaping_cumulative_z"+z+".png")

    plt.show()
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window
