#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 18:02:28 2020

@author: rahch
"""

import matplotlib
#matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.stats import norm
plt.figure(figsize=(38.4,21.6))
plt.rcParams.update({'font.size': 25})
data_Sr90_sigma  = np.load("Results/ResultsSep/data_beta_CDS_1000_sigma.npy")
data_fondo_sigma = np.load("Results/ResultsSep/data_fondo_CDS_1000_sigma.npy")
data_cs137_sigma = np.load("Results/ResultsSep/data_gamma_CDS_1000_sigma.npy")
data_am241_sigma = np.load("Results/ResultsSep/data_alfa_CDS_1000_sigma.npy")
data_Sr90_mean  = np.load("Results/ResultsSep/data_beta_CDS_1000_Mean.npy")
data_fondo_mean = np.load("Results/ResultsSep/data_fondo_CDS_1000_Mean.npy")
data_cs137_mean = np.load("Results/ResultsSep/data_gamma_CDS_1000_Mean.npy")
data_am241_mean = np.load("Results/ResultsSep/data_alfa_CDS_1000_Mean.npy")
data_rx_mean = np.load("Results/ResultsDec/data_RX_CDS_1s_27_11_19_Mean.npy")


dif1 = data_Sr90_mean - data_fondo_mean
dif2 = data_cs137_mean- data_fondo_mean
dif3=  data_am241_mean- data_fondo_mean

dif1s = data_Sr90_sigma - data_fondo_sigma
dif2s = data_cs137_sigma - data_fondo_sigma
dif3s=  data_am241_sigma - data_fondo_sigma
print(np.mean(dif1),np.mean(dif2),np.mean(dif3))
print(np.mean(dif1s),np.mean(dif2s),np.mean(dif3s), np.mean(data_fondo_sigma))
entries, bin_edges, patches =plt.hist(np.ravel(data_Sr90_mean), bins=20, histtype='step', log=True, label='Sr90 background',linewidth=2)
entries1, bin_edges1, patches1 =plt.hist(np.ravel(data_fondo_mean), bins=20, histtype='step', log=True, label='Background WoS',linewidth=2)
entries2, bin_edges2, patches2 =plt.hist(np.ravel(data_cs137_mean), bins=20, histtype='step', log=True, label="Background Cs137",linewidth=2 )
entries3, bin_edges3, patches3 =plt.hist(np.ravel(data_am241_mean), bins=20, histtype='step', log=True, label="Background Am241",linewidth=2)
entries4, bin_edges4, patches4 =plt.hist(np.ravel(data_rx_mean), bins=20, histtype='step', log=True, label="Background Fe55",linewidth=2)
plt.legend()
plt.title("Comparación de medias de los Backgrounds")

plt.savefig("../plots/background_comp.png")
