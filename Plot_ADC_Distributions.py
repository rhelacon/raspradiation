#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 21:30:26 2021

@author: rahch
"""
import matplotlib
#matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.stats import norm
from scipy import stats
plt.figure(figsize=(38.4,21.6))
plt.rcParams.update({'font.size': 25})
parent_dir = "../"
listdir = ["data_beta_CDS_1000adc_distributionFilter_True_5sigma.npz","test_11_crosstalking_M2adc_distributionFilter_False15sigma.npz" ,"data_beta_CDS_1000adc_distributionFilter_True11sigma.npz"]
color_list = ["C0", "C3", "C4", "C2", "C5", "C6", "C1", "C7"]
legend_list=["Sr90 5.0 sigma",  "Sim pair-eh Crosstalking 50", "Sr90 11.0 sigma", "Sr90 13.0 sigma"]
x = np.arange(256)
i=0
data_old = np.zeros(256)
for tmpname in listdir:
    tmp_data = np.load(parent_dir+tmpname)
    data = np.float64(tmp_data.f.arr_0)
    plt.hist(x, bins=256, weights=data, log=True, histtype='step', density=True, color=color_list[i], label=legend_list[i],linewidth=2)
    if i==0:
        data_old =data
    i+=1
    
#aceptance =  np.float64(data_old/np.sum(data_old))/np.float64(data/np.sum(data))
#for j in range(256):
#    print(aceptance[j], ",")
plt.legend()
plt.title("ADC histogram")
plt.xlabel("ADC")
plt.ylabel("frecuency")
plt.savefig("../plot_"+listdir[1]+"adc_distribution_"+"50_recursions"+".png")
plt.show()