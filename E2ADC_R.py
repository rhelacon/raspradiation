#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 13:50:02 2021

@author: mik
"""

from __future__ import division, unicode_literals
from scipy.ndimage.filters import convolve
from colour.utilities import as_float_array, tstack
from colour_demosaicing.bayer import masks_CFA_Bayer
from colour.utilities import as_float_array, tsplit

import colour

from colour_demosaicing import (
    demosaicing_CFA_Bayer_bilinear,
    demosaicing_CFA_Bayer_Malvar2004,
    demosaicing_CFA_Bayer_Menon2007,
    mosaicing_CFA_Bayer)

import numpy as np

###############################################################################

#from io import StringIO
#from subprocess import call

import shlex, subprocess
import os
#import numpy as np

import random
import matplotlib.pyplot as plt
from scipy import stats

from mlxtend.evaluate import permutation_test

###############################################################################
###############################################################################

########################################################################################
########################################################################################
########################################################################################
########################################################################################
 
# Derived Chi Squared Value For This Model
def chi_square(f,y):
    chi2= np.sum( (f-y)*(f-y)/ y )
    return chi2

def chi_square_sigm(f,y,sigma):
    chi2= np.sum(((f-y)/sigma)**2)
    return chi2

def chi2_dist(histA,histB, eps = 1.e-10):
    dist = sum((histA-histB)**2/(histA+histB+eps))
    return 0.5*dist



def RGB_2_YUV(RGB, wb_r=1, wb_b=1):

   # wb_r=(1.+279./256) 
   # wb_b=(1.+535./256) 
   # wb_g=1 
                        
    #Y=np.random.randint(1,10,size=(6,8))
    '''
    RGB= np.array([[7, 4, 4, 9, 3, 7, 4, 1],
          [2, 9, 5, 3, 1, 6, 2, 3],
          [8, 3, 2, 2, 9, 1, 8, 5],
          [4, 3, 2, 4, 4, 8, 1, 7],
          [6, 8, 6, 2, 1, 7, 5, 2],
          [3, 8, 5, 9, 3, 4, 5, 9]])
     
    y=array([[20, 17, 16, 15, 16, 17,  8, 11],
           [19, 16, 10, 13, 16, 16, 13, 13],
           [14,  8,  8, 15, 18, 17, 20, 20],
           [13, 11, 12,  9, 13, 14, 13, 17],
           [22, 22, 17, 12, 12, 16, 16, 17],
           [18, 17, 18, 15, 10, 13, 18, 19]])
    '''
    
    B=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2+1, axis=0)
    B=np.delete(B, np.arange(int(B.shape[1]/2))*2+1, axis=1)
    
    R=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2, axis=0)
    R=np.delete(R, np.arange(int(R.shape[1]/2))*2, axis=1)
    
    G1=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2+1, axis=0)
    G1=np.delete(G1, np.arange(int(G1.shape[1]/2))*2, axis=1)
    
    G2=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2, axis=0)
    G2=np.delete(G2, np.arange(int(G2.shape[1]/2))*2+1, axis=1)
    
    BL=np.insert(B[:,:1],0,B[:,1:].T, axis=1)
    RL=np.insert(R[:,:1],0,R[:,1:].T, axis=1)
    G1L=np.insert(G1[:,:1],0, G1[:,1:].T, axis=1)
    G2L=np.insert(G2[:,:1],0, G2[:,1:].T, axis=1)
    
    BD=np.insert(B[:1,:],0,B[1:,:], axis=0)
    BLD=np.insert(BL[:1,:],0,BL[1:,:], axis=0)
    
    wr=0.299; wg=0.587; wb= 0.114   
    
    Y0 = wr*R*wb_r + wg*G1  + wb*B*wb_b
    Y1 = wr*R*wb_r + wg*G1  + wb*BL*wb_b
    Y2 = wr*R*wb_r + wg*G2  + wb*BD*wb_b
    Y3 = wr*R*wb_r + wg*G2L + wb*BLD*wb_b
    '''
    Y0=R+G1+B
    Y1=R+G1+BL
    Y2=R+G2+BD
    Y3=R+G2L+BLD
    '''
    Y00=np.insert(Y0, np.arange(1,Y0.shape[0]+1),0, axis=0)
    Y00=np.insert(Y00, np.arange(1,Y00.shape[1]+1),0, axis=1)
    
    Y11=np.insert(Y1, np.arange(1,Y1.shape[0]+1),0, axis=0)
    Y11=np.insert(Y11, np.arange(0,Y11.shape[1]),0, axis=1)
    
    Y22=np.insert(Y2, np.arange(0,Y2.shape[0]),0, axis=0)
    Y22=np.insert(Y22, np.arange(1,Y22.shape[1]+1),0, axis=1)
    
    Y33=np.insert(Y3, np.arange(0,Y3.shape[0]),0, axis=0)
    Y33=np.insert(Y33, np.arange(0,Y33.shape[1]),0, axis=1)
    
    #Y = Y00+Y11+Y22+Y33
    Y = np.int16(np.round( Y00+Y11+Y22+Y33, 0 ))
    
    
    b1=np.insert(B, np.arange(1,B.shape[1]+1),0, axis=1)
    b0=np.insert(B, np.arange(0,B.shape[1]),0, axis=1)

    b2=b0+b1

    b1=np.insert(b2, np.arange(1,b2.shape[0]+1),0, axis=0)
    b0=np.insert(b2, np.arange(0,b2.shape[0]),0, axis=0)

    b2=b0+b1

    bL=np.insert(b2[:,:1],0,b2[:,1:].T, axis=1)
    bu=np.insert(bL[:1,:],0,bL[1:,:], axis=0)
    
    umax=0.615
    #U=umax*(bu-Y)/(1-wb)
    U=np.int16(np.round( umax*(bu-Y)/(1-wb)+128 , 0))

    r1=np.insert(R, np.arange(1,R.shape[1]+1),0, axis=1)
    r0=np.insert(R, np.arange(0,R.shape[1]),0, axis=1)

    r2=r0+r1

    r1=np.insert(r2, np.arange(1,r2.shape[0]+1),0, axis=0)
    r0=np.insert(r2, np.arange(0,r2.shape[0]),0, axis=0)

    rv=r0+r1
    vmax=0.615
    #V=vmax*(rv-Y)/(1-wr)
    V=np.int16(np.round( vmax*(rv-Y)/(1-wr)+128 , 0))
    
    #return Y
    return Y, U, V




def RGB_2_Y(RGB, wb_r=1., wb_b=1., bit=8 ):
    nbit=2**bit - 1 
   # wb_r=(1.+279./256) 
   # wb_b=(1.+535./256) 
   # wb_g=1 
                        
    #Y=np.random.randint(1,10,size=(6,8))
    '''
    RGB= np.array([[7, 4, 4, 9, 3, 7, 4, 1],
          [2, 9, 5, 3, 1, 6, 2, 3],
          [8, 3, 2, 2, 9, 1, 8, 5],
          [4, 3, 2, 4, 4, 8, 1, 7],
          [6, 8, 6, 2, 1, 7, 5, 2],
          [3, 8, 5, 9, 3, 4, 5, 9]])
     
    y=array([[20, 17, 16, 15, 16, 17,  8, 11],
           [19, 16, 10, 13, 16, 16, 13, 13],
           [14,  8,  8, 15, 18, 17, 20, 20],
           [13, 11, 12,  9, 13, 14, 13, 17],
           [22, 22, 17, 12, 12, 16, 16, 17],
           [18, 17, 18, 15, 10, 13, 18, 19]])
    '''
    
    B=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2+1, axis=0)
    B=np.delete(B, np.arange(int(B.shape[1]/2))*2+1, axis=1)
    
    R=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2, axis=0)
    R=np.delete(R, np.arange(int(R.shape[1]/2))*2, axis=1)
    
    G1=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2+1, axis=0)
    G1=np.delete(G1, np.arange(int(G1.shape[1]/2))*2, axis=1)
    
    G2=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2, axis=0)
    G2=np.delete(G2, np.arange(int(G2.shape[1]/2))*2+1, axis=1)
    
    BL=np.insert(B[:,:1],0,B[:,1:].T, axis=1)
    RL=np.insert(R[:,:1],0,R[:,1:].T, axis=1)
    G1L=np.insert(G1[:,:1],0, G1[:,1:].T, axis=1)
    G2L=np.insert(G2[:,:1],0, G2[:,1:].T, axis=1)
    
    BD=np.insert(B[:1,:],0,B[1:,:], axis=0)
    BLD=np.insert(BL[:1,:],0,BL[1:,:], axis=0)
    
    wr=0.299; wg=0.587; wb= 0.114   
    
    Y0 = wr*R*wb_r + wg*G1  + wb*B*wb_b
    Y1 = wr*R*wb_r + wg*G1  + wb*BL*wb_b
    Y2 = wr*R*wb_r + wg*G2  + wb*BD*wb_b
    Y3 = wr*R*wb_r + wg*G2L + wb*BLD*wb_b
    '''
    Y0=R+G1+B
    Y1=R+G1+BL
    Y2=R+G2+BD
    Y3=R+G2L+BLD
    '''
    Y00=np.insert(Y0, np.arange(1,Y0.shape[0]+1),0, axis=0)
    Y00=np.insert(Y00, np.arange(1,Y00.shape[1]+1),0, axis=1)
    
    Y11=np.insert(Y1, np.arange(1,Y1.shape[0]+1),0, axis=0)
    Y11=np.insert(Y11, np.arange(0,Y11.shape[1]),0, axis=1)
    
    Y22=np.insert(Y2, np.arange(0,Y2.shape[0]),0, axis=0)
    Y22=np.insert(Y22, np.arange(1,Y22.shape[1]+1),0, axis=1)
    
    Y33=np.insert(Y3, np.arange(0,Y3.shape[0]),0, axis=0)
    Y33=np.insert(Y33, np.arange(0,Y33.shape[1]),0, axis=1)
    
    #Y = Y00+Y11+Y22+Y33
    Y = np.int16(np.round( Y00+Y11+Y22+Y33, 0 ))
    Y = np.int16(np.clip( Y , 0, nbit))

    #return Y
    return Y


########################################################################################  

width = 2592   # 32 * x
height = 1944

y_data_s = np.zeros((height, width))
y_data_s2 = np.zeros((height, width))
y_data_d = np.zeros((height, width))
y_data_a = np.zeros((height, width))
    
z_count =1
nframes = 1001 #

mean0=np.load('/media/mik/UsbStorage/raspradiation-master/Results/ResultsSep/data_beta_CDS_1000_Mean.npy')
sigm0=np.load('/media/mik/UsbStorage/raspradiation-master/Results/ResultsSep/data_beta_CDS_1000_sigma.npy')


tmpname='/media/mik/UsbStorage/CDS_sim/data_beta_CDS_1000/Y_CDS_mean_all.npz'
tmp_data = np.load(tmpname)
mean_all = np.float64(tmp_data.f.arr_0)
tmpname='/media/mik/UsbStorage/CDS_sim/data_beta_CDS_1000/Y_CDS_sigm_all.npz'
tmp_data = np.load(tmpname)
sigm_all = np.float64(tmp_data.f.arr_0)


pname = "/media/mik/UsbStorage/data_15_fer_2021/"
dirname = "data_adc_count"

beta = np.load(pname+dirname+'/' +"Sr90"+"_z000.npz")
beta = np.int64(beta.f.arr_0)

beta_3sig = np.load(pname+dirname+'/' +"Sr90"+"_3sigm"+"_z000.npz")
beta_3sig = np.int64(beta_3sig.f.arr_0)

beta_5sig = np.load(pname+dirname+'/' +"Sr90"+"_5sigm"+"_z000.npz")
beta_5sig = np.int64(beta_5sig.f.arr_0)
                    

path_dir ="/media/mik/UsbStorage/data_15_fer_2021/"


dir_bg = "/media/mik/UsbStorage/CDS_sim/data_fondo_1000_CDS_br50_iso800_15_dic_2020/" 



wb=True
wb=False


strE= 'Emin_'+str(round(emin,3))+'_Emax_'+str(round(emax,3))+'keV'

#dirname = "data_Sr90_pixl_thickZ_"+setpixel_z+"um_sustrat_edep_pixel"
dirname = "data_Sr90_pixl_thickZ_"+setpixel_z+"um_1level_edep_pixel"

dirfolder_lin=path_dir+dirname+convr+"/data"+'_'+setpixel_z+'um'+'_'+strE+convr    
try:
    os.makedirs(dirfolder_lin)
except FileExistsError:
    # directory already exists
    pass

ii=0

s = np.zeros((height, width))
s2 = np.zeros((height, width))

noise_s = np.zeros((height, width))
noise_s2 = np.zeros((height, width))    

for z in range(z_count):
    #Y_CDS_z001_f999
    #nameframe = "E_sim_z" +str(z).zfill(3)+ "_f" #filename format
    nameframe = "Y_CDS_z" +str(z).zfill(3)+ "_f" #filename format
    data_cnt=np.zeros((2, 256),dtype=np.int64)

    y_data_s = np.zeros((height, width))
    y_data_s2 = np.zeros((height, width))
    y_data_d = np.zeros((height, width))
    y_data_a = np.zeros((height, width))

    data_noise_s = np.zeros((height, width))
    data_noise_s2 = np.zeros((height, width))

    for n in range(nframes):
        tmpname = path_dir + dirname + "/" + nameframe + str(n).zfill(3) + ".npz"
        tmp_data = np.load(tmpname)
        tmp_y_data = np.float64(tmp_data.f.arr_0) 

        data=tmp_y_data            

        data_2d =  np.uint16(np.clip((data-emin)*255.0/(emax-emin), 0, 255))####copiar esta linea al algoritmo

        print(convr,setpixel_z, data_2d.min(), data_2d.max(),n)

        if(wb==True):
            '''
            if(wbo=='RGB2Y_wb'):y_data = RGB_2_Y(data_2d)
            if(wbo=='RGB2Y_wb0'):y_data = RGB_2_Y(data_2d, (279./256), (535./256) )
            if(wbo=='RGB2Y_wb1'):y_data = RGB_2_Y(data_2d, (1+279./256), (1+535./256))
            '''
            y_data = RGB_2_Y(data_2d, (1+279./256), (1+535./256))

            print("wb ",int(y_data.min()), int(y_data.max())) 

            #y_data =  np.int16(np.round(y_data, 0))
            y_data = np.round(y_data*abs(sigm_all) + abs(mean_all) , 0)
            y_data = np.int16(np.clip( y_data , 0, 255))


            if(y_data.min()<=0. and y_data.max()<=255):y_data = np.uint(np.clip( (y_data)*255.0/(y_data.max()), 0, 255))
            if(y_data.min()<=0. and y_data.max()>255):y_data = np.uint(np.clip( y_data, 0, 255))
            if(y_data.min()>0.):y_data = np.uint(np.clip( (y_data-y_data.min())*255.0/(y_data.max()-y_data.min()), 0, 255))

            print("wb ",y_data.min(), y_data.max())  

        if(wb==False): y_data = data_2d

        if(n>0 and n % 2 == 0):
            y_data_d = np.int16(y_data)-y_data_a
            #if(noise=="noise"):y_data_d = np.int16(np.clip(y_data_d*sigm_all + mean_all, -255, 255))            
            y_data_s += np.float64(y_data_d)
            y_data_s2 += np.float64(y_data_d)*np.float64(y_data_d)                 

            np.savez_compressed(dirfolder_lin+ "/" +'Y_CDS'+'_'+strE+'_z%03d' % z +'_f%03d' % ii, np.int16(y_data_d1))

            #np.savez_compressed(dirfolder_lin+ "/" +'Y_CDS'+'_'+strE+'_z%03d' % z +'_f%03d' % ii, np.int16(y_data_d))
            print("CDS", n, ii, y_data_d.min(), y_data_d.max())

            ##### add Noise ########   
            #data_noise = np.int16(np.clip(np.round(y_data_d*sigm_all + mean_all, 0), -255, 255))

            noise_file="Y_CDS_z" +str(z).zfill(3)+ "_f"+ str(random.randint(0, 999)).zfill(3)  + ".npz"

            tmp_bg = dir_bg + noise_file
            dat_bg= np.load(tmp_bg)
            dat_bg = np.float64(dat_bg.f.arr_0)

            data_noise = np.int16(np.clip( np.round(y_data_d + dat_bg , 0) , -255, 255))
            print("noise", n, ii, data_noise.min(), data_noise.max(), noise_file)

            data_noise_s += np.float64(data_noise)
            data_noise_s2 += np.float64(data_noise)*np.float64(data_noise)                 
            np.savez_compressed(dirfolder_lin+ "/" + 'Y_CDS_noise'+'_'+strE+'_z%03d' % z +'_f%03d' % ii, np.int16(data_noise))
            print("noise", n, ii, data_noise.min(), data_noise.max())
            ii+=1
            #####################
        y_data_a =  np.int16(y_data)            


        if (n == nframes-1):
            N=0.5*(nframes-1)
            prom=np.float64(y_data_s/N)
            #sigm = sqrt((y_data_s2-N*prom**2)/(N-1))
            sigm = np.float64(((y_data_s2 - N*(prom**2))/(N-1))**0.5)

            #np.save(dirfolder+ "/" +'Y_CDS_sum'+'_z%03d' % k, y_data_s)
            #np.save(dirfolder+ "/" +'Y_CDS_sum2'+'_z%03d' % k, y_data_s2)
            np.savez_compressed(dirfolder_lin+ "/" +'Y_CDS'+'_'+strE+'_mean'+'_z%03d' % z, np.float64(prom))
            np.savez_compressed(dirfolder_lin+ "/" +'Y_CDS'+'_'+strE+'_sigm'+'_z%03d' % z, np.float64(sigm))
            print("ok_CDS", n, N)

            prom_noise=np.float64(data_noise_s/N)
            desv_noise = np.float64(((data_noise_s2 - N*(prom_noise**2))/(N-1))**0.5)

            np.savez_compressed(dirfolder_lin+ "/" +'Y_CDS_noise'+'_'+strE+'_mean'+'_z%03d' % z, np.float64(prom_noise))
            np.savez_compressed(dirfolder_lin+ "/" +'Y_CDS_noise'+'_'+strE+'_sigm'+'_z%03d' % z, np.float64(desv_noise))
            print("ok_noise", z, N)

            ii=0
            print(ii)

    s += y_data_s
    s2 += y_data_s2
    print(y_data_s.mean(),y_data_s2.mean(),s.mean(),s2.mean())

    noise_s += data_noise_s
    noise_s2 += data_noise_s2
    print(data_noise_s.mean(),data_noise_s2.mean(),noise_s.mean(),noise_s2.mean())

    ########################################################################################################
    source ="Sr90"
    nfiles=500
    
    
    #for filtr in ["","_3sigm", "_5sigm" ]:
    for filtr in [ "" ]:
    
        for nois in ["CDS", "CDS_noise"]:  
        #for nois in ["CDS"]:  
            tipo="sim_"+source+'_'+nois
            if(nois == "CDS"):
                filenamef='Y_CDS'+'_'+strE+'_z'
                MeanFrame = prom_all      
                SigmaFrame = desv_all
            if(nois == "CDS_noise"):
                filenamef='Y_CDS_noise'+'_'+strE+'_z'
                MeanFrame = prom_noise_all      
                SigmaFrame = desv_noise_all
    
            print(tipo,filtr)
    
    
            if(filtr == "" ):
                threshold = 0 #filter threshold
                betha = beta
            if(filtr == "_3sigm" ):
                threshold = abs(MeanFrame) +3.0*SigmaFrame #filter threshold
                betha = beta_3sig
            if(filtr == "_5sigm" ):
                threshold = abs(MeanFrame) +5.0*SigmaFrame #filter threshold    
                betha = beta_5sig
    
            dirsave0 = "data_fit_"+setpixel_z+"um"+convr            
            dirsave = dirsave0+"/hist_fit_"+tipo+'_'+setpixel_z+'um'+'_'+strE+convr    
            try:
                os.makedirs(pname+dirsave)
            except FileExistsError:
                pass     
    
            for z in range(z_count):
                data_cnt=np.zeros((2, 256),dtype=np.int64)
    
                for n in range(nfiles):
    
                    tmpname = dirfolder_lin +'/' + filenamef +str(z).zfill(3)+ "_f"+ str(n).zfill(3) + ".npz"
                    tmp_data = np.load(tmpname)
                    data = np.float64(tmp_data.f.arr_0)
                    print(tmpname)
    
                    ev_matrix = abs(data) > threshold #filtering
                    #print(threshold.max())
                    data_f = abs(data)*ev_matrix
    
                    #n_adc, n_count=np.unique(data_f,return_counts=True)
    
                    adc_n = np.unique(data_f,return_counts=True)
    
                    for j in range(adc_n[0].size):
                        data_cnt[0,int(adc_n[0][j])]=(adc_n[0][j])
                        data_cnt[1,int(adc_n[0][j])]=data_cnt[1,int(adc_n[0][j])]+(adc_n[1][j])
    
                    data_cnt[:1].max()
                    data_cnt[1:].max()
                    print(n, data_cnt[:1].max(),data_cnt[1:].max())
    
                np.savez_compressed(pname+dirsave+"/"+tipo+'_'+setpixel_z+'um'+'_'+strE+filtr+"_z"+str(z).zfill(3)+convr+".npz", data_cnt)
                print(pname+dirsave+"/"+tipo+'_'+setpixel_z+'um'+'_'+strE+filtr+"_z"+str(z).zfill(3)+convr+".npz") 
                
                    print(pname+dirsave+"/"+tipo+'_'+setpixel_z+'um'+'_'+strE+filtr+"_z"+str(z).zfill(3)+convr+".npz")        
                        
                ###################################################################################################            
            font_siz=22       
            x0=betha; y0=data_cnt
            
            #densi=True
            #for densi in [False, True]:
            for densi in [False]:
                if(densi==True): histo="_norm"
                if(densi!=True): histo=""
            
                titulo ='data_'+nois+filtr+convr+'_'+setpixel_z+'um_'+strE + histo
            
                bins_x=max(x0[0])+2
                bins_y=max(y0[0])+2
            
                #max_adc=min(max(x0[0])+1, max(y0[0])+1)
                max_adc=256
                min_adc=1
            
                x= x0[:,min_adc:max_adc]
                y= y0[:,min_adc:max_adc]
            
                x_adc, x_weight = np.append(x[0], x[0].max()+1),  x[1]
                bincenters_x = 0.5*(x_adc[1:]+x_adc[:-1])
                norm_x = (np.sum(x_weight) * np.diff(x_adc))
            
                y_adc, y_weight = np.append(y[0], y[0].max()+1),  y[1]
                bincenters_y = 0.5*(y_adc[1:]+y_adc[:-1])
                norm_y = (np.sum(y_weight) * np.diff(y_adc))
            
                if(densi==True):
                    x_w = x[1]/ norm_x
                    y_w = y[1]/ norm_y
                    err_x = np.sqrt(x_weight)/ norm_x
                    err_y = np.sqrt(y_weight)/ norm_y
            
                if(densi==False):
                    x_w = x[1]
                    y_w = y[1]
                    err_x = np.sqrt(x[1])
                    err_y = np.sqrt(y[1])
            
            
                sta_ks, p_ks = stats.ks_2samp(x_w, y_w)
            
                sta_chi, p_chi = stats.chisquare(x_w, y_w)
            
                p_permut = permutation_test(x_w, y_w, method='approximate', num_rounds=10000, seed=0)  #Two-sided permutation test
            
                chi2_d =chi2_dist(x_w, y_w)

                  
                                        
                plt.figure(figsize=(14,8)) 
                
                plt.hist(x[0], weights=x[1], bins=np.arange(bins_x), histtype='step', density=densi, log=True, color='red', linewidth=2.5, 
                         label= source+filtr  )
                plt.hist(y[0], weights=y[1], bins=np.arange(bins_y), histtype='step', density=densi, log=True, color='blue', linewidth=1.5, 
                         label='Sim_'+source+'_'+nois+filtr)
                         #+ '\n P_ks = %.3f' % p_ks + '\n P_ch2 = %.3f' % p_chi + '\n P_prmut = %.3f' % p_permut+ '\n chi2_dist= %.3f' %chi2_dist )  
                
                plt.errorbar(bincenters_x, x_w, yerr=err_x, fmt="r.", ecolor='C3', linewidth=2.5)   
                plt.errorbar(bincenters_y, y_w, yerr=err_x, fmt="b.", ecolor='C0', linewidth=2.5 #) 
                             ,label= '\n P_ks = %.3f' % p_ks + '\n P_prmut = %.3f' % p_permut #)
                             + '\n Chi2_Dist= %.3f' %chi2_d + '\n P_ch2 = %.3f' % p_chi  )                    
                #plt.hist(x[0], weights=expected[0], bins=np.arange(bins_x), histtype='step', density=densi, log=True, color='g', linewidth=1.25, label= "dif"  )
                #plt.hist(x[0], weights=expected[1], bins=np.arange(bins_x), histtype='step', density=densi, log=True, color='c', linewidth=1.25, label= "dif"  )
                    
                #plt.hist(x[0], weights=(x[1]-y[1]), bins=np.arange(bins_x), histtype='step', density=densi, log=True, color='g', linewidth=1.25, label= "dif"  )
                    
                    
                plt.yscale('log')
                plt.grid(True)
                plt.xticks(size=font_siz)
                plt.yticks(size=font_siz)
                plt.xlabel('ADC', fontsize=font_siz)
                plt.ylabel('ADC count in all frames', fontsize=font_siz)
                plt.legend(fontsize=font_siz)
                #plt.title("data_"+source+ filtr +' & ' + tipo + filtr +'_'+setpixel_z+'um'+'_'+strE+'keV' , fontsize=font_siz)
                plt.title(titulo , fontsize=font_siz)
                #plt.title("data"+convr+'_'+setpixel_z+'um'+'_'+strE+'keV' , fontsize=font_siz)
                plt.tight_layout()
                    
                plt.savefig(pname+dirsave+"/"+"plot_hist_"+histo+tipo+'_'+setpixel_z+'um'+'_'+strE+filtr+convr+".png", dpi=150)
                plt.show()
                plt.clf()
                plt.close()
                temp_parm=np.array([nois+filtr, pm, emin, emax, p_ks, p_chi])
                print(nois+filtr, pm, emin, emax, p_ks, p_chi)
                parm_tes = np.vstack((parm_tes, temp_parm))
######################################3        
#parm_tes=np.zeros((5, 10))
        #parm_tes[0][i] = pm
        #parm_tes[1][i] = emin
        #parm_tes[2][i] = emax
        #parm_tes[3][i] = p_ks
        #parm_tes[4][i] = p_chi
    #np.save(pname+dirsave0+"/"+"parm_test_fit_"+setpixel_z+"um"+'_'+strE+convr+".npy", parm_tes.T)