#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 23:30:28 2021

@author: rahch
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import *
from scipy.stats import norm
from scipy.stats import uniform

def pairs(Ekin, edep):
    int_data = np.loadtxt('/home/rahch/Workspace/Raspcam/DATA/Data_simu_Agosto_5_2021/plot-data.csv', skiprows=1, delimiter=',' )
    int_data_s = int_data[int_data[:,0].argsort()]
    x = int_data_s[:,0]
    y =  int_data_s[:,1]
    lin_inter = interp1d(x,y, kind='linear')
    
    Etotal = 1000000.0*(Ekin + 0.51099895000)
    if Etotal>10000:
        epsilon= 3.63
    else:
        epsilon = lin_inter(Etotal)
    mean_pair = edep*1000.0/epsilon
    Fano = 0.35
    #print(mean_pair, edep*1000)
    return np.abs(int(np.floor(norm.rvs(loc=mean_pair, scale= Fano*mean_pair,  size=1))))

def crosstalking(Y_tmp, counter):
    Y = np.float64(Y_tmp)
    width = 2592
    height = 1944
    excess = 0.0
    j,i= np.where(Y > 4300)

    for k in range(len(j)):
         excess = Y[j[k],i[k]] - 4300.0
         excess = excess/10.0
         if(j[k]+1<1944):
             Y[j[k]+1,i[k]]+= excess
             Y[j[k]+1,i[k]-1]+= excess
             if i[k]+1 <2592:
                 Y[j[k]+1,i[k]+1]+= excess
             
         else:
             excess = excess + excess
             
         if (i[k]+1 < 2592):
             Y[j[k],i[k]+1]+= excess
             Y[j[k]-1,i[k]+1]+= excess
             Y[j[k]-1,i[k]-1]+= excess
         else:
             excess = excess + excess
         Y[j[k],i[k]-1]+= excess
         Y[j[k]-1,i[k]]+= excess
    
    j,i= np.where(Y > 4300)

    for k in range(len(j)):
         
         if(j[k]+1<1944):
             Y[j[k]+1,i[k]]+= excess/6.0
             Y[j[k]+1,i[k]-1]+= excess/6.0
             if i[k]+1 <2592:
                 Y[j[k]+1,i[k]+1]+= excess/6.0
             
         else:
             excess = excess + excess/2.0
             
         if (i[k]+1 < 2592):
             Y[j[k],i[k]+1]+= excess/6.0
             Y[j[k]-1,i[k]+1]+= excess/6.0
             Y[j[k]-1,i[k]-1]+= excess/6.0
         else:
             excess = excess + excess/2.0
         Y[j[k],i[k]-1]+= excess/6.0
         Y[j[k]-1,i[k]]+= excess/6.0
    
    #random crosstalk
#    j,i= np.where(Y > 0)
   
    """    for k in range(len(j)):
        j_tmp = 0
        i_tmp = 0
        share = 0 
        x,y,s = uniform.rvs(size=3)
        try:
        
            if y < 0.2:
                j_tmp = j - 1
                if x < 0.2:
                    i_tmp = i- 1
                    share = np.floor(s*Y[j,i]/3.0)
                    Y[j,i] = Y[j,i] -3*share
                    Y[j_tmp, i_tmp]+= share
                    Y[j_tmp +1 ,i_tmp]+=share
                    Y[j_tmp +1 ,i_tmp +1]+=share
                elif x > 0.8:
                    i_tmp = i+1
                    share = np.floor(s*Y[j,i]/3.0)
                    Y[j,i] = Y[j,i] -3*share
                    Y[j_tmp, i_tmp]+= share
                    Y[j_tmp +1 ,i_tmp]+=share
                    Y[j_tmp  ,i_tmp -1]+=share
                else:
                    i_tmp = i
                    share = np.floor(s*Y[j,i]/3.0)
                    Y[j,i] = Y[j,i] -3*share
                    Y[j_tmp -1, i_tmp]+= share
                    Y[j_tmp -1 ,i_tmp+1]+=share
                    Y[j_tmp -1 ,i_tmp-1]+=share
            elif y > 0.8 :
                j_tmp =j+ 1
                if x < 0.2:
                    i_tmp = i- 1
                    share = np.floor(s*Y[j,i]/3.0)
                    Y[j,i] = Y[j,i] -3*share
                    Y[j_tmp, i_tmp]+= share
                    Y[j_tmp -1 ,i_tmp]+=share
                    Y[j_tmp  ,i_tmp +1]+=share
                elif x > 0.8:
                    i_tmp = i+1
                    share = np.floor(s*Y[j,i]/3.0)
                    Y[j,i] = Y[j,i] -3*share
                    Y[j_tmp, i_tmp]+= share
                    Y[j_tmp -1 ,i_tmp]+=share
                    Y[j_tmp  ,i_tmp -1]+=share
                else:
                    i_tmp = i
                    share = np.floor(s*Y[j,i]/3.0)
                    Y[j,i] = Y[j,i] -3*share
                    Y[j_tmp , i_tmp]+= share
                    Y[j_tmp ,i_tmp+1]+=share
                    Y[j_tmp ,i_tmp-1]+=share
            
            else:
                j_tmp=j
                if x < 0.2:
                    i_tmp = i- 1
                    share = np.floor(s*Y[j,i]/3.0)
                    Y[j,i] = Y[j,i] -3*share
                    Y[j_tmp, i_tmp]+= share
                    Y[j_tmp -1 ,i_tmp]+=share
                    Y[j_tmp +1  ,i_tmp]+=share
                elif x > 0.8:
                    i_tmp = i+1
                    share = np.floor(s*Y[j,i]/3.0)
                    Y[j,i] = Y[j,i] -3*share
                    Y[j_tmp, i_tmp]+= share
                    Y[j_tmp -1 ,i_tmp]+=share
                    Y[j_tmp +1  ,i_tmp]+=share
                else:
                    j_tmp=j
        except IndexError as error:
            
            pass
    return np.int64(Y)
    """
         
    if counter > 10:
        
        return np.int64(Y)
    else:
        l = np.where(Y>4500.0)
    
        if len(l) != 0 :
        
          Y = crosstalking(Y ,counter+1)
    return np.int64(np.floor(Y))
        
   
width = 2592
height = 1944
dirname="/home/rahch/Workspace/Raspcam/DATA/Data_simu_Agosto_5_2021/data_Sr90_pixl_thickZ_10um_distSD_0mm_10level_csv_tar_bz/"
cds = 0
back_dir ="/home/rahch/Workspace/Raspcam/DATA/Data_Septiembre_2019/data_fondo_CDS_1000"
#noise statistical
noise_data_dir = "/home/rahch/Workspace/Raspcam/Scripts/rasprad/Results/ResultsSep"
noise_stat = np.zeros((height,width), dtype=int)
noise_means = np.load(noise_data_dir+"/data_beta_CDS_1000_Mean.npy")
noise_stdev = np.load(noise_data_dir+"/data_beta_CDS_1000_sigma.npy")

       
for nfile in range(1000):
    filename0 = dirname +'pixel'+str(nfile)+'_SD0mm_Sr90_edep_all_electron_arrived_detector.csv'
    all_edep = np.loadtxt(filename0, skiprows=1, usecols=(0,1,2,3,4,6,7,8))
    filename1 = dirname+ 'pixel'+str(nfile)+'_SD0mm_Sr90_electron_edep2D.csv'
    pos_edep = np.loadtxt(filename1, skiprows=1)
#ordenar por energia depositada ambos arreglos
    all_edep_sorted = all_edep[all_edep[:,0].argsort()]
    pos_edep_sorted = pos_edep[pos_edep[:,2].argsort()]
    cam = np.zeros((height,width),dtype=int)
    r_matrix = np.zeros((height,width),dtype=int)
    g0_matrix = np.zeros((height,width),dtype=int)
    g1_matrix = np.zeros((height,width),dtype=int)
    b_matrix = np.zeros((height,width),dtype=int)
#cubic spline failling
    k=0

    for i in range(pos_edep_sorted[:,1].size):
        j= np.where(all_edep_sorted[:,0]== pos_edep_sorted[i,2])
    
        if j[0].size ==1 :
            npairs = pairs(all_edep_sorted[j,3], all_edep_sorted[j,0])
              
        if j[0].size>1:
            if k >= j[0].size:
                k=0
                npairs = pairs(all_edep_sorted[j[0][k],3], all_edep_sorted[j[0][k],0])
                k+=1
                
        cam[int(pos_edep_sorted[i,1]),int(pos_edep_sorted[i,0])]+= npairs
    cam = crosstalking(cam, 0)
    cam = np.uint16(np.clip((cam)*255.0/(4300.0), 0, 255))    
#desdoblar a RGB para convertir a YUV

    wb_r = 1.0 + 279.0/256.0
    wb_b= 1.0 +535.0/256.0
    RGB = cam
    B=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2+1, axis=0)
    B=np.delete(B, np.arange(int(B.shape[1]/2))*2+1, axis=1)
    
    R=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2, axis=0)
    R=np.delete(R, np.arange(int(R.shape[1]/2))*2, axis=1)
    
    G1=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2+1, axis=0)
    G1=np.delete(G1, np.arange(int(G1.shape[1]/2))*2, axis=1)
    
    G2=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2, axis=0)
    G2=np.delete(G2, np.arange(int(G2.shape[1]/2))*2+1, axis=1)
    
    BL=np.insert(B[:,:1],0,B[:,1:].T, axis=1)
    RL=np.insert(R[:,:1],0,R[:,1:].T, axis=1)
    G1L=np.insert(G1[:,:1],0, G1[:,1:].T, axis=1)
    G2L=np.insert(G2[:,:1],0, G2[:,1:].T, axis=1)
    
    BD=np.insert(B[:1,:],0,B[1:,:], axis=0)
    BLD=np.insert(BL[:1,:],0,BL[1:,:], axis=0)
    
    wr=0.299; wg=0.587; wb= 0.114   
    
    Y0 = wr*R*wb_r + wg*G1  + wb*B*wb_b
    Y1 = wr*R*wb_r + wg*G1  + wb*BL*wb_b
    Y2 = wr*R*wb_r + wg*G2  + wb*BD*wb_b
    Y3 = wr*R*wb_r + wg*G2L + wb*BLD*wb_b
    '''
    Y0=R+G1+B
    Y1=R+G1+BL
    Y2=R+G2+BD
    Y3=R+G2L+BLD
    '''
    Y00=np.insert(Y0, np.arange(1,Y0.shape[0]+1),0, axis=0)
    Y00=np.insert(Y00, np.arange(1,Y00.shape[1]+1),0, axis=1)
    
    Y11=np.insert(Y1, np.arange(1,Y1.shape[0]+1),0, axis=0)
    Y11=np.insert(Y11, np.arange(0,Y11.shape[1]),0, axis=1)
    
    Y22=np.insert(Y2, np.arange(0,Y2.shape[0]),0, axis=0)
    Y22=np.insert(Y22, np.arange(1,Y22.shape[1]+1),0, axis=1)
    
    Y33=np.insert(Y3, np.arange(0,Y3.shape[0]),0, axis=0)
    Y33=np.insert(Y33, np.arange(0,Y33.shape[1]),0, axis=1)
    
    #Y = Y00+Y11+Y22+Y33
    Y = np.int16(np.round( Y00+Y11+Y22+Y33, 0 ))
    Y = np.int16(np.clip( Y , 0, 255))
    #statistical  noise
    noise_stat = norm.rvs(size=height*width).reshape(height,width)
    noise_stat = noise_stat*noise_stdev + np.abs(noise_means)
    #inserting statistical noise
    cam = cam + noise_stat
    cam= np.floor(np.clip( cam , 0, 255))
    cam = np.int16(np.clip(np.floor(Y*noise_stdev +np.abs(noise_means)),0,255)) #weird renormalization
    #cam = Y
    
    
    #inserting statistical noise
    index = int((nfile-1)/2)
    
    if cds ==1:
        filenamef = "Y_CDS_z" +str(0).zfill(3)+ "_f"
        tmpname = back_dir + "/" + filenamef + str(index).zfill(3) + ".npz"
        tmp_data = np.load(tmpname)
        data = np.int32(tmp_data.f.arr_0)
        #cam = cam + data
        cam = np.int32(np.clip(cam, 0, 255))
        np.savez_compressed(dirname+"../test_11_crosstalking_M2/" + "Y_CDS_" +"z" +str(0).zfill(3)+ "_f"+str(index).zfill(3) + ".npz", cam-cam0)
        print("saving: " + "Y_CDS_" +"z" +str(0).zfill(3)+ "_f"+str(index).zfill(3) + ".npz")
        cds = 0
    else:
        cam0= cam
        cds+=1