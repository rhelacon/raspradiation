#!/usr/bin/env python3

import numpy as np
import matplotlib
#matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
import scipy
import math
from scipy.optimize import curve_fit
from scipy.special import factorial
from scipy.optimize import minimize
from scipy.stats import poisson
from scipy.stats import norm
import cv2
import peakutils
import seaborn as sns
import pandas as pd
import os
import pickle
from scipy import stats   
# poisson function, parameter lamb is the fit parameter
def poisson(k, lamb):
    return (np.float_power(lamb,k)/factorial(k, exact=False))*np.exp(-lamb)

def Ngauss(x0,mu, sigma):
    A0=1.0/(sigma*np.sqrt(2.0*np.pi))
    x= (np.float_power(x0-mu,2.0))/(2.0*np.float_power(sigma,2.0))
    return A0*np.exp(-x)

def Dgauss(x0,mu0, sigma0,A, mu1,sigma1,B):
    A0=A/(sigma0*np.sqrt(2.0*np.pi))
    x= (np.float_power(x0-mu0,2.0))/(2.0*np.float_power(sigma0,2.0))
    B0=B/(sigma1*np.sqrt(2.0*np.pi))
    y= (np.float_power(x0-mu1,2.0))/(2.0*np.float_power(sigma1,2.0))
    return A0*np.exp(-x) + B0*np.exp(-y)

def Tgauss(x0,mu0, sigma0,A, mu1,sigma1,B,mu2,sigma2,C):
    A0=A/(sigma0*np.sqrt(2.0*np.pi))
    x= (np.float_power(x0-mu0,2.0))/(2.0*np.float_power(sigma0,2.0))
    B0=B/(sigma1*np.sqrt(2.0*np.pi))
    y= (np.float_power(x0-mu1,2.0))/(2.0*np.float_power(sigma1,2.0))
    C0=C/(sigma2*np.sqrt(2.0*np.pi))
    z= (np.float_power(x0-mu2,2.0))/(2.0*np.float_power(sigma2,2.0))
    return A0*np.exp(-x) + B0*np.exp(-y) + C0*np.exp(-z)
    
def negLogLikelihood(params, data):
    """ the negative log-Likelohood-Function"""
    lnl = - np.sum(np.log(poisson(data, params[0])))
    return lnl
    
    
# exponential function, parameters y0, a, b  are the fit parameters    
def expdecay(x,y0,a,b):
    return y0+a*np.exp(-b*x)

def expdecay1(x,a,b):
    return a*np.exp(-b*x)

def invr2_0(x,a, b):
    return a/np.float_power(x+b,2.0)
        

def invr2_1(x,a,y0):
    if (x != 0.0):
        return y0+a/np.float_power(x,2.0)
    else :
        x=x+0.00000000001
        return y0+a/np.float_power(x,2.0)

def chi2(o,e):
    chi=np.float_power(o -e, 2.0)/np.absolute(e)
    return np.sum(chi)

def chi2_sigma(o,e, sigma):
    chi=np.float_power(o -e, 2.0)/np.power(sigma,2.0)
    return np.sum(chi)

def ad_test(data0, data1):
    ad, cv, val = stats.anderson_ksamp([data0, data1])
    return ad, cv[2] 

def plot_z(hts,z,dirn):   
    plt.figure(figsize=(38.4,21.6))
    plt.rcParams.update({'font.size': 45})
    entries, bin_edges, patches = plt.hist(hts,bins=10000, range=[-0.5, 100000.5], density=True)
    indexes = peakutils.indexes(entries, thres=0.5, min_dist=3)
    print(indexes)
    bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])
    m0=np.mean(hts)
    sg=np.std(hts, ddof=1)
    sg=sg/2.0
    x_plot = np.linspace(0, 100000, 10000000)
    res_str = ' '
    # fit with curve_fit
    if (dirn=="data_beta_CDS_1000"):
       try:
            parameters, cov_matrix = curve_fit(Ngauss, bin_middles, entries,p0=[bin_edges[indexes[0]],np.std(hts, ddof=1)]) 
            parameters = abs(parameters)
       except:
           parameters = np.zeros(1)
       #if (z == 19):
       #    plt.cla()   # Clear axis
       #    plt.clf()   # Clear figure
           #plt.close() # Close a figure window
       #    plt.figure(figsize=(38.4,21.6))
       #    plt.rcParams.update({'font.size': 45})
       #    entries, bin_edges, patches = plt.hist(hts,bins=101, range=[-0.5, 100.5], density=True)
       #    bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])
       #    indexes = peakutils.indexes(entries, thres=0.5, min_dist=2)
       #    parameters, cov_matrix = curve_fit(poisson, bin_middles, entries,p0=[bin_edges[indexes[0]]]) 
           #parameters = abs(parameters)
       #    res_str = '$\lambda_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])#+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
       #    x_plot = np.linspace(0, 100, 100000)
        #   plt.plot(x_plot, poisson(x_plot, *parameters), 'r-', lw=6)
       #    y = poisson(x_plot, *parameters)
           #y_index = peakutils.indexes(y, 0.5, min_dist=2)
       #else :
       if (parameters[0]<20):
                plt.cla()   # Clear axis
                plt.clf()   # Clear figure
                #plt.close() # Close a figure window
                plt.figure(figsize=(38.4,21.6))
                plt.rcParams.update({'font.size': 45})
                entries, bin_edges, patches = plt.hist(hts,bins=101, range=[-0.5, 100.5], density=True)
                bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])
                indexes = peakutils.indexes(entries, thres=0.5, min_dist=2)
                parameters, cov_matrix = curve_fit(poisson, bin_middles, entries,p0=[bin_edges[indexes[0]]]) 
                #parameters = abs(parameters)
                res_str = '$\lambda_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])#+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
                x_plot = np.linspace(0, 100, 100000)
                plt.plot(x_plot, poisson(x_plot, *parameters), 'r-', lw=6)
                y = poisson(x_plot, *parameters)
                #y_index = peakutils.indexes(y, 0.5, min_dist=2)
       else:
                res_str = '$\mu_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+'\n'+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
                plt.plot(x_plot, Ngauss(x_plot, *parameters), 'r-', lw=6)
                y = Ngauss(x_plot, *parameters)
                #y_index = peakutils.indexes(y, 0.5, min_dist=2)
                      
    else:    
        if (indexes.size == 0):
            parameters, cov_matrix = curve_fit(Ngauss, bin_middles, entries,p0=[np.max(entries),np.std(hts, ddof=1)]) 
            parameters = abs(parameters)
            res_str = '$\mu_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+'\n'+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
            plt.plot(x_plot, Ngauss(x_plot, *parameters), 'r-', lw=6)
            y=Ngauss(x_plot, *parameters)
            if (parameters[0]<20):
                mu_tmp = parameters[0]
                plt.cla()   # Clear axis
                plt.clf()   # Clear figure
                #plt.close() # Close a figure window
                plt.figure(figsize=(38.4,21.6))
                plt.rcParams.update({'font.size': 45})
                entries, bin_edges, patches = plt.hist(hts,bins=101, range=[-0.5, 100.5], density=True)
                bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])
                parameters, cov_matrix = curve_fit(poisson, bin_middles, entries,p0=[mu_tmp]) 
                #parameters = abs(parameters)
                res_str = '$\lambda_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])#+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
                x_plot = np.linspace(0, 100, 100000)
                plt.plot(x_plot, poisson(x_plot, *parameters), 'r-', lw=6)
                y = poisson(x_plot, *parameters)
        if (indexes.size == 1):
            parameters, cov_matrix = curve_fit(Ngauss, bin_middles, entries,p0=[bin_edges[indexes[0]],np.std(hts, ddof=1)]) 
            parameters = abs(parameters)
            res_str = '$\mu_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+'\n'+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
            plt.plot(x_plot, Ngauss(x_plot, *parameters), 'r-', lw=6)
            y=Ngauss(x_plot, *parameters)
            if (parameters[0]<20):
                mu_tmp = parameters[0]
                plt.cla()   # Clear axis
                plt.clf()   # Clear figure
                #plt.close() # Close a figure window
                plt.figure(figsize=(38.4,21.6))
                plt.rcParams.update({'font.size': 45})
                entries, bin_edges, patches = plt.hist(hts,bins=101, range=[-0.5, 100.5], density=True)
                bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])
                parameters, cov_matrix = curve_fit(poisson, bin_middles, entries,p0=[mu_tmp]) 
                #parameters = abs(parameters)
                res_str = '$\lambda_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])#+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
                x_plot = np.linspace(0, 100, 100000)
                plt.plot(x_plot, poisson(x_plot, *parameters), 'r-', lw=6)
                y = poisson(x_plot, *parameters)
        if (indexes.size == 2):  
            try:
                parameters, cov_matrix = curve_fit(Dgauss, bin_middles, entries,p0=[bin_edges[indexes[0]], 50, entries[indexes[0]], bin_edges[indexes[1]], 50, entries[indexes[1]]] )
                parameters = abs(parameters)
                res_str = '$\mu_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+'\n'+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])+'\n'+'$A_0$ ='+'%.3f'%parameters[2] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[2]) +'\n'+'$\mu_1$ ='+'%.3f'%parameters[3] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[3])+'\n'+'$\sigma_1$ ='+'%.3f'%parameters[4] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[4])+'\n'+'$A_1$ ='+'%.3f'%parameters[5]+ ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[5])
                plt.plot(x_plot, Dgauss(x_plot, *parameters), 'r-', lw=6)
                y = Dgauss(x_plot, *parameters)
            except RuntimeError:
                parameters, cov_matrix = curve_fit(Ngauss, bin_middles, entries,p0=[bin_edges[indexes[0]],np.std(hts, ddof=1)]) 
                parameters = abs(parameters)
                res_str = '$\mu_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+'\n'+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
                plt.plot(x_plot, Ngauss(x_plot, *parameters), 'r-', lw=6)
                y = Ngauss(x_plot, *parameters)
        if (indexes.size > 2): 
            try:
                parameters, cov_matrix = curve_fit(Tgauss, bin_middles, entries,p0=[bin_edges[indexes[0]], 50, entries[indexes[0]], bin_edges[indexes[1]], 50, entries[indexes[1]],bin_edges[indexes[2]], 50, entries[indexes[2]]] )
                parameters = abs(parameters)
                res_str = '$\mu_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+'\n'+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])+'\n'+'$A_0$ ='+'%.3f'%parameters[2] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[2]) +'\n'+'$\mu_1$ ='+'%.3f'%parameters[3] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[3])+'\n'+'$\sigma_1$ ='+'%.3f'%parameters[4] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[4])+'\n'+'$A_1$ ='+'%.3f'%parameters[5] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[5])+'\n'+'$\mu_2$ ='+'%.3f'%parameters[6] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[6])+'\n'+'$\sigma_2$ ='+'%.3f'%parameters[7] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[7])+'\n'+'$A_2$ ='+'%.3f'%parameters[8] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[8])
                plt.plot(x_plot, Tgauss(x_plot, *parameters), 'r-', lw=6)
                y = Tgauss(x_plot, *parameters)
                            
            except RuntimeError:
                parameters, cov_matrix = curve_fit(Dgauss, bin_middles, entries,p0=[bin_edges[indexes[0]], 50, entries[indexes[0]], bin_edges[indexes[1]], 50, entries[indexes[1]]] )
                parameters = abs(parameters)
                res_str = '$\mu_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+'\n'+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])+'\n'+'$A_0$ ='+'%.3f'%parameters[2] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[2]) +'\n'+'$\mu_1$ ='+'%.3f'%parameters[3] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[3])+'\n'+'$\sigma_1$ ='+'%.3f'%parameters[4] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[4])+'\n'+'$A_1$ ='+'%.3f'%parameters[5]+ ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[5])
                plt.plot(x_plot, Dgauss(x_plot, *parameters), 'r-', lw=6)
                y = Dgauss(x_plot, *parameters)
            except:
                parameters, cov_matrix = curve_fit(Ngauss, bin_middles, entries,p0=[bin_edges[indexes[0]],np.std(hts, ddof=1)]) 
                parameters = abs(parameters)
                res_str = '$\mu_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+'\n'+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
                plt.plot(x_plot, Ngauss(x_plot, *parameters), 'r-', lw=6)
                y = Ngauss(x_plot, *parameters)
            
    #print(parameters, np.sqrt(np.diag(cov_matrix)))
    #result = minimize(negLogLikelihood,  # function to minimize
    #              x0=np.ones(1),     # start value
    #              args=(hits_ts,),      # additional arguments for function
    #              method='Powell',   # minimization method, see docs
    #              )
# result is a scipy optimize result object, the fit parameters 
# are stored in result.x
    #print(result)
    
# plot poisson-deviation with fitted parameter
    if(math.isnan(np.max(y))):
        ymax= np.max(entries)*1.6
    else:    
        if(np.max(y) > np.max(entries)):
            ymax = np.max(y)
            #print(ymax, np.max(entries))
        else:
            ymax = np.max(entries)
            #print(ymax, np.max(y))
    average = np.average(bin_middles, weights=entries)
    variance = np.average((bin_middles - average)**2, weights=entries)
    plt.xlabel('Clusters')
    plt.ylabel('Frecuency')
    plt.ylim(0, ymax*1.5)
    plt.xlim(0, average + 10.0*np.sqrt(variance))
    plt.title('Cluster distribution for Z= ' + str(z))
    
    plt.text(average + 1.5*np.sqrt(variance), 0.5*ymax, res_str)
    plt.grid(True)
    plt.savefig("../"+dirn+"/"+dirn+"Hist_z"+str(z)+"_Clgauss.png")
    data = np.column_stack((entries, bin_edges))
    np.save("../"+dirn+"/"+dirn+"Hist_z"+str(z)+"_Clgauss.npy", data)
    #plt.show()
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window
    #print(bin_edges.size, entries.size, bin_middles.size)
    
    return average, np.sqrt(variance)

def plot_z2(hts,z,dname, lbl, filtr=False):
    if (hts.size == 0):
        print("no data in: " +dname +" "+lbl)
        return 0, 0
    plt.figure(figsize=(19.2,10.8))
    plt.rcParams.update({'font.size': 35})
    ax = plt.subplot(111)
    
    m0=np.mean(hts)
    sg=np.std(hts, ddof=1)
    tmp_m0 = m0
    tmp_sg = sg
    print("mean: " , m0)
    print("std_dev : ", sg)
    res_str = " "
    data = np.copy(hts)
    #for i in range(data.size):
    #   print(data[i])
    
    if(filtr):
        q1= np.quantile(hts, 0.25)
        q3= np.quantile(hts, 0.75)
        iqr =scipy.stats.iqr(hts)
        #print(q1, q3, iqr, np.max(hts))
        condition = ((hts <= (q3 + 1.8*iqr)) & (hts >= (q1- 1.8*iqr)))
        hts_f = np.extract(condition, hts)
        print("Z: ", z," Filtered: ", ((hts.size - hts_f.size)/hts.size)*100, "%")
    else:
        hts_f= hts
    m0=np.mean(hts_f)
    sg=np.std(hts_f, ddof=1)
        
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'+lbl
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'+lbl
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'+lbl
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'+lbl
    else:
        isotope=lbl
    if (m0 < sg):
    #Freedman Diaconi's rule
        iqr= scipy.stats.iqr(hts_f)
        FDR= 2*iqr/np.cbrt(hts_f.size)
        if FDR != 0: 
            nbins=np.int(np.ceil(np.max(hts_f)/FDR))
        else:
            FDR = np.int(np.ceil(np.cbrt(hts_f.size)*2.0))
            nbins=np.int(np.ceil(np.max(hts_f)/FDR))
        
    else:
    #Scott's rule for bin size
        h=3.5*sg/np.cbrt(hts_f.size)
        if (h > 0.0):
            nbins=np.int(np.ceil(np.max(hts_f)/h))
        else:
            #rice  rule for number of bins
            nbins= np.int(np.ceil(np.cbrt(hts_f.size)*2.0))
    if(nbins == 0) :
        print("size:", hts_f.size)
        print("max:", np.max(hts_f))
    entries, bin_edges, patches = plt.hist(hts_f,bins=nbins, density=True,label=isotope)
    bin_middles = 0.5*(bin_edges[1:] + bin_edges[:-1])
    indexes = peakutils.indexes(entries, thres=0.5, min_dist=1)
    if (indexes.size > 0) :
        tmp_mu = bin_middles[indexes[0]]
    else:
        tmp_mu = np.average(bin_middles, weights=entries)
    try:
        if ( m0 > 20 and sg != 0.0):
            print("sigma:", sg)
            parameters, cov_matrix = curve_fit(Ngauss, bin_middles, entries,p0=[m0,sg])
            Gauss0=True
            Gauss=True
        else:
            Gauss0=False
    except RunTimeError:
         Gauss=False
         print("problem fitting gaussian")
    except OptimizeWarning:
         Gauss=False
         print("problem fitting gaussian")
    if (Gauss0):
            #parameters, cov_matrix = curve_fit(Ngauss, bin_middles, entries,p0=[m0,sg]) 
            print("is Gauss")
            print(parameters)
            parameters = abs(parameters)
            sample = stats.norm.rvs(loc=parameters[0], scale=parameters[1] ,size=hts.size)
            ks, p_value= stats.ks_2samp(hts, sample)
            print("KS: ", ks)
            print("P-value:", p_value )
            x_plot = np.linspace(0, 10000000, 10000000)
            res_str = '$\mu_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+'\n'+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1]) + ' \n' + 'KS P-value: ' + '%.3f'%p_value 
            try:
                plt.plot(x_plot, Ngauss(x_plot, *parameters), 'r-', lw=6)
                y=Ngauss(x_plot, *parameters)
            except:
                pass
            
            Gauss=True
            Poisson=False
    else:
            try:
                if(sg != 0.0): 
                    parameters, cov_matrix = curve_fit(poisson, bin_middles, entries,p0=[m0]) 
                    #parameters = abs(parameters)
                    if(isinstance(parameters[0], float)):
                        if (~np.isinf(np.diag(cov_matrix))[0]):
                            Poisson=True
                            Gauss=False
                        else:
                            Gauss=False
                            Poisson=False
                else:
                    Gauss= False
                    Poisson = False
                   
            except RunTimeError:
                Gauss=False
                Poisson=False
            except OptimizeWarning:
                Gauss=False
                Poisson=False
            except RunTimeWarning:
                Gauss=False
                Poisson=False
            
            if(Poisson):
                sample = stats.poisson.rvs(parameters[0], size=hts.size)
                ks, p_value = ad_test(hts, sample)
                res_str = '$\lambda_0$ ='+'%.3f'%parameters[0] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0]) + '\n' + 'Anderson-Darling:  ' + '%.3f'%ks + '\n' + "Critic value: " + '%.3f'%p_value   #+'$\sigma_0$ ='+'%.3f'%parameters[1] + ' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])
                x_plot = np.linspace(0, 100, 100000)
                plt.plot(x_plot, poisson(x_plot, *parameters), 'r-', lw=6)
                y = poisson(x_plot, *parameters)
                Gauss=False
    if(Gauss==False and Poisson == False ):
            ymax = np.max(entries) 
    else:        
        if(np.max(y) > np.max(entries)):
            ymax = np.max(y)
            #print(ymax, np.max(entries))
        else:
            ymax = np.max(entries)
            #print(ymax, np.max(y))
    if (Gauss):
        tmp_sg = parameters[1]
        max_x = parameters[0]+ 10*tmp_sg
    elif(Poisson):   
        tmp_sg= np.sqrt(parameters[0])
        max_x = parameters[0]+ 10*tmp_sg
    else:
        max_x = 1.1*np.max(hts_f)
    
        
    
    plt.ylim(0, ymax*1.5)
    plt.xlim(0, max_x)
    plt.title('Cluster distribution for Z= ' + str(z))
    
    plt.text(tmp_mu + 1.5*tmp_sg, 0.5*ymax, res_str)
    plt.grid(True)
    plt.savefig("../"+dname+"/"+dname+"Hist_z"+str(z)+"_Clgauss"+lbl+".png")
    
    np.save("../"+dname+"/"+dname+"Hist_z"+str(z)+"_Clgauss"+lbl+".npy", data)
    #with open("../"+dname+"/"+dname+"Hist_z"+str(z)+"_Clgauss"+lbl+".pkl",'wb') as fid:
        #pickle.dump(ax, fid)
        #fid.close()
    #plt.show()
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window
    
    return m0, sg
    
def plot_nz(hmean,sh,zc,dname, lbl): 
    z = np.arange(0.0,zc*2.0,2.0, dtype=np.float64)
    plt.figure(figsize=(38.4,21.6))
    plt.rcParams.update({'font.size': 35})
    
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'+lbl
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'+lbl
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'+lbl
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'+lbl
    else:
        isotope= lbl
    plt.errorbar(z,hmean ,yerr=sh, fmt='bo',label="Clusters avg. "+isotope,capsize=30.0 , ms=10.0)
    plt.title('Z vs Clusters multiplicity '+ isotope)
    plt.ylabel('Number of Clusters')
    plt.xlabel('Z (mm)')
    plt.ylim(0, np.max(hmean)*2.0)
    plt.xlim(-1.0, np.max(z)*1.3)
    z_plot = np.linspace(0, (zc-1)*2.0, 1000)
    exp0_failure = False
    exp1_failure = False
    invr2_failure = False
    st_exp= ' '
    st_exp1= ' '
    st_invr2= ' '
    try:
        parameters, cov_matrix = curve_fit(expdecay, z, hmean,p0=[0.0, hmean[0], 1.0])
    except RuntimeError:
        st_exp= ' '
        exp0_failure = True
    except ValueError:
        st_exp= ' '
        exp0_failure = True
    except OptimizeWarning:
        st_exp= ' '
        exp0_failure = True
    if(not exp0_failure):
        jiexp= chi2_sigma(hmean,expdecay(z, *parameters),sh)/(hmean.size - 1.0 - parameters.size)
        st_exp = r'$f(x) = y_0 + Ae^{-\mu x}$'+'\n' + '$y_0 =$'+'%.3f'%parameters[0] +' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[0])+ '\n' + '$A=$'+'%.3f'%parameters[1] +' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[1])+'\n'+ '$\mu =$'+'%.3f'%parameters[2] + '$\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix)[2]) +'\n' +'$\chi^2 /ddof =$'+'%.3f'%jiexp  +'\n'
        exp0_failure = False

    try: 
        parameters0, cov_matrix0 = curve_fit(expdecay1, z, hmean,p0=[ hmean[0], 1.0], sigma=sh)
    except RuntimeError:
        st_exp1= ' '
        exp1_failure = True
    except ValueError:
        st_exp1= ' '
        exp1_failure = True
    except OptimizeWarning:
        st_exp1= ' '
        exp1_failure = True
    if(not exp1_failure): 
        jiexp1= chi2_sigma(hmean,expdecay1(z, *parameters0),sh)/(hmean.size - 1.0 -parameters0.size)
        st_exp1 = r'$f(x) =Ae^{-\mu x}$'+'\n' + '$A=$'+'%.3f'%parameters0[0] +' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix0)[0])+'\n'+ '$\mu =$'+'%.3f'%parameters0[1] + '$\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix0)[1]) +'\n' +'$\chi^2 / ddof =$'+'%.3f'%jiexp1  +'\n'
        exp1_failure = False
      
    try:
        parameters1, cov_matrix1 = curve_fit(invr2_0, z, hmean,p0=[ hmean[0], 0.01],sigma=sh)
        
    except RuntimeError:
        st_invr2= '1/z^2 failed '
        print('1/r2 failed')
        invr2_failure = True
    except ValueError:
        st_invr2= '1/z^2 failed '
        invr2_failure = True
        print('1/r2 failed')
    if (not invr2_failure):
        jiinvr2= chi2_sigma(hmean,invr2_0(z, *parameters1),sh)/(hmean.size - parameters1.size)
        st_invr2 =  r'$f(x) =  A/(x + B)^2$'+'\n' + '$A=$'+'%.3f'%parameters1[0] +' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix1)[0]) + '\n'+ '$B=$'+'%.3f'%parameters1[1] +' $\pm$'+'%.3f'%np.sqrt(np.diag(cov_matrix1)[1]) + '\n'+'$\chi^2 / ddof =$'+'%.3f'%jiinvr2
        invr2_failure = False
    #print(parameters, np.sqrt(np.diag(cov_matrix)))
    if(exp0_failure== False):
        plt.plot(z_plot, expdecay(z_plot, *parameters), 'r-', lw=5, label="Exponential Decay 1")
    if(exp1_failure == False):
        plt.plot(z_plot, expdecay1(z_plot, *parameters0), 'g--', lw=5, label="Exponential Decay 2")
    if(invr2_failure == False):
        plt.plot(z_plot, invr2_0(z_plot, *parameters1), 'm-.', lw=5, label="Inverse-square law")
    plt.grid(True)
    plt.legend(loc='upper left')
    plt.text(0.7*np.max(z),0.9*np.max(hmean), st_exp + st_exp1 + st_invr2)
    plt.savefig("../"+dname+"/"+dname+"NCl_vs_Z"+lbl+"_expdecay.png")
    data = np.column_stack((z,hmean,sh))
    np.save("../"+dname+"/"+dname+"NCl_vs_Z"+lbl+"_expdecay.npy", data)
    #plt.show()
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window
    
def plotMultiplicityHisto(data,z, dname ,maxcs):
    if data.size  == 0:
        return 0
    plt.figure(figsize=(19.2,10.8))
    plt.rcParams.update({'font.size': 30})
    ax = plt.subplot(111)
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'
    else:
        isotope= dname
    max_clsize = maxcs
    #Freedman Diaconi's rule
    iqr= scipy.stats.iqr(data)
    FDR= 2*iqr/np.cbrt(data.size)
    if FDR != 0:
        nbins=np.int(np.ceil(np.max(data)/FDR))
    else:
        
    #Scott's rule
        sigma=np.std(data, ddof=1)
        h=3.5*sigma/np.cbrt(data.size)
        nbins=np.int(np.ceil(np.max(data)/h))
    #entries, bin_edges, patches = plt.hist(range(max_clsize),bins=range(max_clsize +1), weights=frec, log=True)
    entries, bin_edges, patches = plt.hist(data,bins=nbins , log=True , histtype='step')
    plt.title('Clusters multiplicity for '+ isotope +'Z: ' + str(z*2.0) + 'mm')
    plt.ylabel('Counts')
    plt.xlabel('Cluster size')
    plt.xlim(0,maxcs*1.10) 
    plt.grid(True)
    plt.savefig("../"+dname+"/"+"ClMultiplicity"+dname + "z_"+str(z)+ ".png")
    #data = np.column_stack((entries, bin_edges))
    np.save("../"+dname+"/"+"ClMultiplicity"+dname + "z_"+str(z)+ ".npy", data)
    #with open("../"+dname+"/"+"ClMultiplicity"+dname + "z_"+str(z)+ ".pickle",'wb') as fid:
        #pickle.dump(ax,fid)
        #fid.close()
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window

def PlotSeedHisto(data,z, dname , extra , bits10):
    if data.size == 0:
        return 0
    plt.figure(figsize=(19.2,10.8))
    plt.rcParams.update({'font.size': 40})
    ax= plt.subplot(111)
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'
    else:
        isotope= dname
    print("data size:", data.size)
    if bits10:
        max_adc =1024
    else:
        max_adc = 256
    if (data.size != 0):
    
    #entries, bin_edges, patches = plt.hist(range(max_clsize),bins=range(max_clsize +1), weights=frec, log=True)
        entries, bin_edges, patches = plt.hist(data,bins=range(max_adc), log=True, label=isotope+"z: "+str(z*2.0)+ "mm", histtype='step')
        plt.title('Seed Histogram for '+ isotope +'Z: ' + str(z*2.0) + 'mm')
        plt.ylabel('Counts')
        plt.xlabel('Seed ADC Value')
        plt.xlim(-2,np.max(data)*1.10)
        plt.grid(True)
        plt.savefig("../"+dname+"/"+"SeedHisto"+dname + "z_"+str(z)+ extra+".png")
        #with open("../"+dname+"/"+"SeedHisto"+dname + "z_"+str(z)+ extra+".pickle", 'wb') as fid: 
        #    pickle.dump(ax, fid )
        #    fid.close()
        np.save("../"+dname+"/"+"SeedHisto"+dname + "z_"+str(z)+ extra+".npy", data)
        plt.cla()   # Clear axis
        plt.clf()   # Clear figure
        plt.close() # Close a figure window
    
def PlotClMeanHisto(data,z, dname , extra, bits10 ):
    if data.size == 0:
        return 0
    plt.figure(figsize=(19.2,10.8))
    plt.rcParams.update({'font.size': 40})
    ax = plt.subplot(111)
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'
    else:
        isotope= dname
    if bits10:
        max_adc =1024
    else:
        max_adc = 256
    #entries, bin_edges, patches = plt.hist(range(max_clsize),bins=range(max_clsize +1), weights=frec, log=True)
    entries, bin_edges, patches = plt.hist(data,bins=range(max_adc), log=True, label=isotope+"z: "+str(z*2.0)+ "mm", histtype='step')
    plt.title('Cluster Mean Histogram for '+ isotope +'Z: ' + str(z*2.0) + 'mm')
    plt.ylabel('Counts')
    plt.xlabel('Mean ADC Value')
    plt.xlim(0,np.max(data)*1.10)
    plt.grid(True)
    plt.savefig("../"+dname+"/"+"MeanEHisto"+dname + "z_"+str(z)+ extra+".png")
    #with open("../"+dname+"/"+"MeanEHisto"+dname + "z_"+str(z)+ extra+".pickle", 'wb') as fid:
         #pickle.dump(ax,fid)
         #fid.close()
    np.save("../"+dname+"/"+"MeanEHisto"+dname + "z_"+str(z)+ extra+".npy", data)
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window
    
def PlotClTotalEHisto(data,z, dname , extra, log=False ):
    if data.size == 0:
        return 0
    plt.figure(figsize=(19.2,10.8))
    plt.rcParams.update({'font.size': 40})
    ax = plt.subplot(111)
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'
    else:
        isotope= dname
    
    #entries, bin_edges, patches = plt.hist(range(max_clsize),bins=range(max_clsize +1), weights=frec, log=True)
    #Sturge's rule
    
    #nbins = np.int(np.ceil( 1 + 3.322*math.log10(data.size))) 
    #Freedman Diaconi's rule
    #iqr= scipy.stats.iqr(data)
    #FDR= 2*iqr/np.cbrt(data.size)
    #nbins=np.int(np.ceil(np.max(data)/FDR))
    #Scott's rule
    sigma=np.std(data, ddof=1)
    h=3.5*sigma/np.cbrt(data.size)
    nbins=np.int(np.ceil(np.max(data)/h))
    if log:
        hist, bins = np.histogram(data, bins=nbins)
        logbins = np.logspace(np.log10(bins[0]),np.log10(bins[-1]),len(bins))
        plt.hist(data, bins=logbins, label=isotope, histtype='step')
        plt.xscale('log')
        plt.yscale('log')
        tmp_filename = "../"+dname+"/"+"TotalEHisto"+dname + "z_"+str(z)+ extra+"log.png"
    #print(bins)
    else:   
        entries, bin_edges, patches = plt.hist(data,bins=nbins, log=True,  label=isotope, histtype='step')
        plt.yscale('log')
        tmp_filename = "../"+dname+"/"+"TotalEHisto"+dname + "z_"+str(z)+ extra+".png"
    #print(logbins)
   
   
    plt.title('Cluster Total Energy for '+ isotope +'Z: ' + str(z*2.0) + 'mm')
    plt.ylabel('Counts')
    plt.xlabel('ADCs')
    plt.xlim(0,np.max(data)*1.10) 
    plt.grid(True)
    plt.savefig(tmp_filename)
    #with open(tmp_filename.replace(".png", ".pickle"), 'wb') as fid:
            #pickle.dump(ax,fid)
            #fid.close()
    np.save(tmp_filename.replace(".png", ".npy"), data)
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window
    
def PlotScatterBubble(Seed, Cluster, z, dname, lbl):      
    plt.figure(figsize=(19.2, 10.8))
    plt.rcParams.update({'font.size': 40})
    #sns.set(style="white")
    ClusterInfo = pd.DataFrame()
    ClusterInfo["Seed"]=Seed
    ClusterInfo["ClusterSize"]=Cluster
    ztmp= str(z)
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'
    else:
        isotope= "unknown"
    if(dname=="data_RX_CDS_1s_2cm_15_11_19"):
        isotope=r'$Fe^{55}$'
        ztmp = "2cm"
        
    ax = sns.relplot(x="Seed", y="ClusterSize", Sizes=(4,200), alpha=0.5, data=ClusterInfo, height=9, aspect=1.77778)
    plt.title(isotope +" z: "+ ztmp)
    plt.xlim(0,np.max(Seed)*1.10)
    plt.ylim(0,np.max(Cluster)*1.10) 
    plt.xlabel("SeedSignal")
    plt.ylabel("ClusterSize")
    #ax.set_label(isotope)
    #ax._legend.set_title("isotopes")
    #new_labels = ['Cs137']
    #for t, l in zip(g._legend.texts, new_labels): t.set_text(l)
    plt.savefig("../"+dname+"/"+lbl+dname +"Bubbleplot"+ "z_"+str(z)+".png")
    #plt.show()
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window
    
def PlotSeedClusterSize(Seed, Cluster, z, dname, lbl1, lbl2,lbl3): 
    plt.figure(figsize=(19.2, 10.8))
    plt.rcParams.update({'font.size': 40})
    ax = plt.subplot(111)
    #sns.set(style="white")

    ztmp = str(2*z)
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'
    else:
        isotope= dname
    if(dname=="data_RX_CDS_1s_2cm_15_11_19"):
        isotope=r'$Fe^{55}$'
        ztmp = "2cm"
        
    #ax = sns.scatterplot(x=Seed, y=Cluster, alpha=0.5)
    plt.hist2d(Seed, Cluster,bins=(np.int(np.ceil(np.max(Seed))),np.int(np.ceil(np.max(Cluster)))), range=([0, np.max(Seed)],[0, np.max(Cluster)]) , norm=matplotlib.colors.LogNorm(), cmap=plt.cm.jet)
    #plt.hist2d(Seed, Cluster, bins=(256,np.max(Cluster) ), norm=matplotlib.colors.LogNorm())
    plt.title(isotope +" z: "+ ztmp +"mm")
    plt.xlim(0,np.max(Seed)*1.10)
    plt.ylim(0,np.max(Cluster)*1.10) 
    plt.xlabel(lbl1)
    plt.ylabel(lbl2)
    #ax._legend.set_title("isotopes")
    #new_labels = ['Cs137']
    #for t, l in zip(g._legend.texts, new_labels): t.set_text(l)

    plt.colorbar()
    plt.savefig("../"+dname+"/"+lbl1+" vs "+ lbl2 +"_"+lbl3+dname +"his2dlog"+ "z_"+str(z)+".png")
    data = np.column_stack((Seed, Cluster))
    np.save("../"+dname+"/"+lbl1+" vs "+ lbl2 +"_"+lbl3+dname +"his2dlog"+ "z_"+str(z)+".npy", data)
    #with open("../"+dname+"/"+lbl1+" vs "+ lbl2 +"_"+lbl3+dname +"his2dlog"+ "z_"+str(z)+".pickle", 'wb') as fid:
    #     pickle.dump(ax,fid )
    #     fid.close()
    
    #plt.show()
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window
    
    
def PlotSigmaHisto(data,z, dname , extra, log=False ):
    plt.figure(figsize=(19.2,10.8))
    plt.rcParams.update({'font.size': 40})
    ax = plt.subplot(111)
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'
    else:
        isotope= dname
    
    #entries, bin_edges, patches = plt.hist(range(max_clsize),bins=range(max_clsize +1), weights=frec, log=True)
    #Sturge's rule
    
    #nbins = np.int(np.ceil( 1 + 3.322*math.log10(data.size))) 
    #Freedman Diaconi's rule
    #iqr= scipy.stats.iqr(data)
    #FDR= 2*iqr/np.cbrt(data.size)
    #nbins=np.int(np.ceil(np.max(data)/FDR))
    #Scott's rule
    sigma=np.std(data, ddof=1)
    h=3.5*sigma/np.cbrt(data.size)
    nbins=np.int(np.ceil(np.max(data)/h))
    if log:
        hist, bins = np.histogram(data, bins=nbins)
        logbins = np.logspace(np.log10(bins[0]),np.log10(bins[-1]),len(bins))
        plt.hist(data, bins=logbins, label=isotope, histtype='step')
        plt.xscale('log')
        plt.yscale('log')
        tmp_filename = "../"+dname+"/"+"SigmaHisto"+dname + "z_"+str(z)+ extra+"log.png"
    #print(bins)
    else:   
        entries, bin_edges, patches = plt.hist(data,bins=nbins, log=True,  label=isotope, histtype='step')
        plt.yscale('log')
        tmp_filename = "../"+dname+"/"+"SigmaHisto"+dname + "z_"+str(z)+ extra+".png"
    #print(logbins)
   
   
    plt.title('Standar deviation of clusters for '+ isotope +'Z: ' + str(z*2.0) + 'mm')
    plt.ylabel('Counts')
    plt.xlabel('ADCs')
    plt.xlim(0,np.max(data)*1.10) 
    plt.grid(True)
    plt.savefig(tmp_filename)
    #with open(tmp_filename.replace(".png", ".pickle"), 'wb') as fid:
            #pickle.dump(ax,fid)
            #fid.close()
    np.save(tmp_filename.replace(".png", ".npy"), data)
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window

def PlotShapeHisto(data,z, dname , extra, log=False ):
    plt.figure(figsize=(19.2,10.8))
    plt.rcParams.update({'font.size': 40})
    ax = plt.subplot(111)
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'
    else:
        isotope= dname
    
    #entries, bin_edges, patches = plt.hist(range(max_clsize),bins=range(max_clsize +1), weights=frec, log=True)
    #Sturge's rule
    
    #nbins = np.int(np.ceil( 1 + 3.322*math.log10(data.size))) 
    #Freedman Diaconi's rule
    iqr= scipy.stats.iqr(data)
    FDR= 2*iqr/np.cbrt(data.size)
    if FDR != 0:
        nbins=np.int(np.ceil(np.max(data)/FDR))
    else:
        nbins=50000
    #Scott's rule
    #sigma=np.std(data, ddof=1)
    #h=3.5*sigma/np.cbrt(data.size)
    #nbins=np.int(np.ceil(np.max(data)/h))
    #nbins=500
    if log:
        hist, bins = np.histogram(data, bins=nbins)
        logbins = np.logspace(np.log10(bins[0]),np.log10(bins[-1]),len(bins))
        plt.hist(data, bins=logbins, label=isotope, histtype='step',linewidth=2)
        plt.xscale('log')
        plt.yscale('log')
        tmp_filename = "../"+dname+"/"+"ShapeHisto"+dname + "z_"+str(z)+ extra+"log.png"
    #print(bins)
    else:   
        entries, bin_edges, patches = plt.hist(data,bins=nbins,range=(np.min(data), np.max(data)) ,log=True,  label=isotope, histtype='step',linewidth=2)
        plt.yscale('log')
        tmp_filename = "../"+dname+"/"+"ShapeHisto"+dname + "z_"+str(z)+ extra+".png"
    #print(logbins)
   
   
    plt.title('Shaping of clusters for '+ isotope +'Z: ' + str(z*2.0) + 'mm')
    plt.ylabel('Counts')
    plt.xlabel('Arbitrary Units')
    #plt.xlim(np.min(data)*1.10,np.max(data)*1.10) 
    plt.xlim(np.min(data)*0.9,np.max(data)*1.10) 
    plt.grid(True)
    plt.savefig(tmp_filename)
    #with open(tmp_filename.replace(".png", ".pickle"), 'wb') as fid:
            #pickle.dump(ax,fid)
            #fid.close()
    np.save(tmp_filename.replace(".png", ".npy"), data)
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window
    
def PlotDensHisto(data,z, dname , extra, log=False ):
    plt.figure(figsize=(19.2,10.8))
    plt.rcParams.update({'font.size': 40})
    ax = plt.subplot(111)
    if((dname.find("RX") != -1) or (dname.find("Fe55") != -1)):
        isotope= r'$Fe^{55}$'
    elif((dname.find("beta") != -1) or (dname.find("Sr90") != -1)):
        isotope=r'$Sr^{90}$'
    elif((dname.find("gamma") != -1) or (dname.find("Cs137") != -1)):
        isotope= r'$Cs^{137}$'
    elif((dname.find("alfa") != -1) or (dname.find("Am241") != -1)):
        isotope= r'$Am^{241}$'
    else:
        isotope= dname
    
    #entries, bin_edges, patches = plt.hist(range(max_clsize),bins=range(max_clsize +1), weights=frec, log=True)
    #Sturge's rule
    
    #nbins = np.int(np.ceil( 1 + 3.322*math.log10(data.size))) 
    #Freedman Diaconi's rule
    iqr= scipy.stats.iqr(data)
    FDR= 2*iqr/np.cbrt(data.size)
    if FDR != 0:
        nbins=np.int(np.ceil(np.max(data)/FDR))
    else:
        nbins=50000
    #Scott's rule
    #sigma=np.std(data, ddof=1)
    #h=3.5*sigma/np.cbrt(data.size)
    #nbins=np.int(np.ceil(np.max(data)/h))
    #nbins=500
    if log:
        hist, bins = np.histogram(data, bins=nbins)
        logbins = np.logspace(np.log10(bins[0]),np.log10(bins[-1]),len(bins))
        plt.hist(data, bins=logbins, label=isotope, histtype='step',linewidth=2)
        plt.xscale('log')
        plt.yscale('log')
        tmp_filename = "../"+dname+"/"+"DensHisto"+dname + "z_"+str(z)+ extra+"log.png"
    #print(bins)
    else:   
        entries, bin_edges, patches = plt.hist(data,bins=nbins,range=(np.min(data), np.max(data)) ,log=True,  label=isotope, histtype='step',linewidth=2)
        plt.yscale('log')
        tmp_filename = "../"+dname+"/"+"DensHisto"+dname + "z_"+str(z)+ extra+".png"
    #print(logbins)
   
   
    plt.title('Density of clusters for '+ isotope +'Z: ' + str(z*2.0) + 'mm')
    plt.ylabel('Counts')
    plt.xlabel('Arbitrary Units')
    #plt.xlim(np.min(data)*1.10,np.max(data)*1.10) 
    plt.xlim(np.min(data)*0.9,np.max(data)*1.10) 
    plt.grid(True)
    plt.savefig(tmp_filename)
    #with open(tmp_filename.replace(".png", ".pickle"), 'wb') as fid:
            #pickle.dump(ax,fid)
            #fid.close()
    np.save(tmp_filename.replace(".png", ".npy"), data)
    plt.cla()   # Clear axis
    plt.clf()   # Clear figure
    plt.close() # Close a figure window