#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 18 00:05:48 2021

@author: rahch
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 13 11:49:17 2021

@author: rahch
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 23:30:28 2021

@author: rahch
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import *
from scipy.stats import norm

def pairs(Ekin, edep):
    int_data = np.loadtxt('/home/rahch/Workspace/Raspcam/DATA/Data_simu_Agosto_5_2021/plot-data.csv', skiprows=1, delimiter=',' )
    int_data_s = int_data[int_data[:,0].argsort()]
    x = int_data_s[:,0]
    y =  int_data_s[:,1]
    lin_inter = interp1d(x,y, kind='linear')
    
    Etotal = 1000000.0*(Ekin + 0.51099895000)
    if Etotal>10000:
        epsilon= 3.63
    else:
        epsilon = lin_inter(Etotal)
    mean_pair = edep*1000.0/epsilon
    Fano = 0.35
    #print(mean_pair, edep*1000)
    npairs = abs(int(np.floor(norm.rvs(loc=mean_pair, scale= Fano*mean_pair,  size=1))))
    scale =4300/255
    ADC = np.int(np.floor(npairs/scale))
    #charge collection efficiency through MC
    return ADC

def MC_TUNNING(Y):
    height, width = Y.shape
    acc_arr = np.array([1.0013983289759192 , 0.0 ,
                        7.908889591901297e-06 ,
                        0.00034252290072983723 ,
                        0.0010448975272962947 ,
                        0.0038375387047895116 ,
                        0.007001203605217488 ,
                        0.015318582445831803 ,
                        0.030676633336569267 ,
                        0.04897003875376772 ,
                        0.08980113084276473 ,
                        0.12012187405698489 ,
                        0.17209302325581396 ,
                        0.2949842883390284 ,
                        0.3040977249224405 ,
                        0.41903576851695634 ,
                        0.49556833718353693 ,
                        0.5736285668051136 ,
                        0.7103895227165327 ,
                        0.6994531807220732 ,
                        0.9227220826672757 ,
                        0.82467916244512 ,
                        0.8966849207901428 ,
                        1.054036419716472 ,])
    for i in range(height):
        for j in range( width):
            ADC = np.int(Y[i,j])
            if  ADC < 24:
            
                dice = np.random.random_sample()
                if dice > acc_arr[ADC]:
                   #ADC = pairs(Ekin, edep)
                   Y[i,j] = 0
                  
    Y_new = Y            
    return Y_new 




width = 2592
height = 1944
dirname="/home/rahch/Workspace/Raspcam/DATA/Data_simu_Agosto_5_2021/data_Sr90_pixl_thickZ_10um_distSD_0mm_10level_csv_tar_bz/"
cds = 0
back_dir ="/home/rahch/Workspace/Raspcam/DATA/Data_Septiembre_2019/data_fondo_CDS_1000"
#noise statistical
noise_data_dir = "/home/rahch/Workspace/Raspcam/Scripts/rasprad/Results/ResultsSep"
noise_stat = np.zeros((height,width), dtype=int)
noise_means = np.load(noise_data_dir+"/data_beta_CDS_1000_Mean.npy")
noise_stdev = np.load(noise_data_dir+"/data_beta_CDS_1000_sigma.npy")

       
for nfile in range(1000):
    filename0 = dirname +'pixel'+str(nfile)+'_SD0mm_Sr90_edep_all_electron_arrived_detector.csv'
    all_edep = np.loadtxt(filename0, skiprows=1, usecols=(0,1,2,3,4,6,7,8))
    filename1 = dirname+ 'pixel'+str(nfile)+'_SD0mm_Sr90_electron_edep2D.csv'
    pos_edep = np.loadtxt(filename1, skiprows=1)
#ordenar por energia depositada ambos arreglos
    all_edep_sorted = all_edep[all_edep[:,0].argsort()]
    pos_edep_sorted = pos_edep[pos_edep[:,2].argsort()]
    cam = np.zeros((height,width),dtype=int)
    r_matrix = np.zeros((height,width),dtype=int)
    g0_matrix = np.zeros((height,width),dtype=int)
    g1_matrix = np.zeros((height,width),dtype=int)
    b_matrix = np.zeros((height,width),dtype=int)
#cubic spline failling
    k=0

    for i in range(pos_edep_sorted[:,1].size):
        j= np.where(all_edep_sorted[:,0]== pos_edep_sorted[i,2])
    
        if j[0].size ==1 :
            ADC = pairs(all_edep_sorted[j,3], all_edep_sorted[j,0])
            
            if ADC < 0 :
                print(ADC, npairs)
            
           
        if ADC > 255:
            ADC =255
        
        if j[0].size>1:
            if k >= j[0].size:
                k=0
                ADC = pairs(all_edep_sorted[j[0][k],3], all_edep_sorted[j[0][k],0])
                k+=1
                if ADC > 255:
                    ADC =255
        cam[int(pos_edep_sorted[i,1]),int(pos_edep_sorted[i,0])]+= ADC
        if cam[int(pos_edep_sorted[i,1]),int(pos_edep_sorted[i,0])] > 255:
            cam[int(pos_edep_sorted[i,1]),int(pos_edep_sorted[i,0])] = 255
#desdoblar a RGB para convertir a YUV

    wb_r = 1.0 + 279.0/256.0
    wb_b= 1.0 +535.0/256.0
    RGB = cam
    B=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2+1, axis=0)
    B=np.delete(B, np.arange(int(B.shape[1]/2))*2+1, axis=1)
    
    R=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2, axis=0)
    R=np.delete(R, np.arange(int(R.shape[1]/2))*2, axis=1)
    
    G1=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2+1, axis=0)
    G1=np.delete(G1, np.arange(int(G1.shape[1]/2))*2, axis=1)
    
    G2=np.delete(RGB, np.arange(int(RGB.shape[0]/2))*2, axis=0)
    G2=np.delete(G2, np.arange(int(G2.shape[1]/2))*2+1, axis=1)
    
    BL=np.insert(B[:,:1],0,B[:,1:].T, axis=1)
    RL=np.insert(R[:,:1],0,R[:,1:].T, axis=1)
    G1L=np.insert(G1[:,:1],0, G1[:,1:].T, axis=1)
    G2L=np.insert(G2[:,:1],0, G2[:,1:].T, axis=1)
    
    BD=np.insert(B[:1,:],0,B[1:,:], axis=0)
    BLD=np.insert(BL[:1,:],0,BL[1:,:], axis=0)
    
    wr=0.299; wg=0.587; wb= 0.114   
    
    Y0 = wr*R*wb_r + wg*G1  + wb*B*wb_b
    Y1 = wr*R*wb_r + wg*G1  + wb*BL*wb_b
    Y2 = wr*R*wb_r + wg*G2  + wb*BD*wb_b
    Y3 = wr*R*wb_r + wg*G2L + wb*BLD*wb_b
    '''
    Y0=R+G1+B
    Y1=R+G1+BL
    Y2=R+G2+BD
    Y3=R+G2L+BLD
    '''
    Y00=np.insert(Y0, np.arange(1,Y0.shape[0]+1),0, axis=0)
    Y00=np.insert(Y00, np.arange(1,Y00.shape[1]+1),0, axis=1)
    
    Y11=np.insert(Y1, np.arange(1,Y1.shape[0]+1),0, axis=0)
    Y11=np.insert(Y11, np.arange(0,Y11.shape[1]),0, axis=1)
    
    Y22=np.insert(Y2, np.arange(0,Y2.shape[0]),0, axis=0)
    Y22=np.insert(Y22, np.arange(1,Y22.shape[1]+1),0, axis=1)
    
    Y33=np.insert(Y3, np.arange(0,Y3.shape[0]),0, axis=0)
    Y33=np.insert(Y33, np.arange(0,Y33.shape[1]),0, axis=1)
    
    #Y = Y00+Y11+Y22+Y33
    Y = np.int16(np.round( Y00+Y11+Y22+Y33, 0 ))
    Y = np.int16(np.clip( Y , 0, 255))
    
    cam = MC_TUNNING(Y)
    
    
#    cam = cam + noise_stat
    #inserting statistical noise
    index = int((nfile-1)/2)
    
    if cds ==1:
        filenamef = "Y_CDS_z" +str(0).zfill(3)+ "_f"
        tmpname = back_dir + "/" + filenamef + str(index).zfill(3) + ".npz"
        tmp_data = np.load(tmpname)
        data = np.int32(tmp_data.f.arr_0)
        #cam = cam + data
        cam = np.int32(np.clip(cam, 0, 255))
        np.savez_compressed(dirname+"../test_08_MC_Y/" + "Y_CDS_" +"z" +str(0).zfill(3)+ "_f"+str(index).zfill(3) + ".npz", cam-cam0)
        print("saving: " + "Y_CDS_" +"z" +str(0).zfill(3)+ "_f"+str(index).zfill(3) + ".npz")
        cds = 0
    else:
        cam0= cam
        cds+=1