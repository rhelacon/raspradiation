#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 22 14:19:02 2021

@author: rahch
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import *
from scipy.stats import norm

def pairs(Ekin, edep):
    int_data = np.loadtxt('/home/rahch/Workspace/Raspcam/DATA/Data_simu_Agosto_5_2021/plot-data.csv', skiprows=1, delimiter=',' )
    int_data_s = int_data[int_data[:,0].argsort()]
    x = int_data_s[:,0]
    y =  int_data_s[:,1]
    lin_inter = interp1d(x,y, kind='linear')
    
    Etotal = 1000000.0*(Ekin + 0.51099895000)
    if Etotal>10000:
        epsilon= 3.63
    else:
        epsilon = lin_inter(Etotal)
    mean_pair = edep*1000.0/epsilon
    Fano = 0.117
    #print(mean_pair, edep*1000)
    return abs(int(np.floor(norm.rvs(loc=mean_pair, scale= np.sqrt(Fano*mean_pair),  size=1))))

width = 2592
height = 1944
dirname="/home/rahch/Workspace/Raspcam/DATA/Data_simu_Agosto_5_2021/data_Sr90_pixl_thickZ_10um_distSD_0mm_10level_csv_tar_bz/"
cds = 0
back_dir ="/home/rahch/Workspace/Raspcam/DATA/Data_Septiembre_2019/data_fondo_CDS_1000"
nbits=10
range_bits = 2**nbits -1
#noise statistical
noise_data_dir = "/home/rahch/Workspace/Raspcam/Scripts/rasprad/Results/ResultsSep"
noise_stat = np.zeros((height,width), dtype=int)
noise_means = np.load(noise_data_dir+"/data_beta_CDS_1000_Mean.npy")
noise_stdev = np.load(noise_data_dir+"/data_beta_CDS_1000_sigma.npy")

       
for nfile in range(1000):
    filename0 = dirname +'pixel'+str(nfile)+'_SD0mm_Sr90_edep_all_electron_arrived_detector.csv'
    all_edep = np.loadtxt(filename0, skiprows=1, usecols=(0,1,2,3,4,6,7,8))
    filename1 = dirname+ 'pixel'+str(nfile)+'_SD0mm_Sr90_electron_edep2D.csv'
    pos_edep = np.loadtxt(filename1, skiprows=1)
#ordenar por energia depositada ambos arreglos
    all_edep_sorted = all_edep[all_edep[:,0].argsort()]
    pos_edep_sorted = pos_edep[pos_edep[:,2].argsort()]
    cam = np.zeros((height,width),dtype=int)
    r_matrix = np.zeros((height,width),dtype=int)
    g0_matrix = np.zeros((height,width),dtype=int)
    g1_matrix = np.zeros((height,width),dtype=int)
    b_matrix = np.zeros((height,width),dtype=int)
#cubic spline failling
    k=0

    for i in range(pos_edep_sorted[:,1].size):
        j= np.where(all_edep_sorted[:,0]== pos_edep_sorted[i,2])
    
        if j[0].size ==1 :
            npairs = pairs(all_edep_sorted[j,3], all_edep_sorted[j,0])
            scale =4300/range_bits
            ADC = np.int(np.floor(npairs/scale))
        if ADC > range_bits:
            ADC = range_bits
        
        if j[0].size>1:
            if k >= j[0].size:
                k=0
                npairs = pairs(all_edep_sorted[j[0][k],3], all_edep_sorted[j[0][k],0])
                k+=1
                scale =4300/range_bits
                ADC = np.int(np.floor(npairs/scale))
                if ADC > range_bits:
                    ADC = range_bits
        cam[int(pos_edep_sorted[i,1]),int(pos_edep_sorted[i,0])]+= ADC
        if cam[int(pos_edep_sorted[i,1]),int(pos_edep_sorted[i,0])] > 255:
            cam[int(pos_edep_sorted[i,1]),int(pos_edep_sorted[i,0])] = 255
#desdoblar a RGB para convertir a YUV


    
#    cam = cam + noise_stat
    #inserting statistical noise
    index = int((nfile-1)/2)
    
    if cds ==1:
        filenamef = "Y_CDS_z" +str(0).zfill(3)+ "_f"
        tmpname = back_dir + "/" + filenamef + str(index).zfill(3) + ".npz"
        tmp_data = np.load(tmpname)
        data = np.int32(tmp_data.f.arr_0)
        cam = cam + data
        cam = np.int32(np.clip(cam, 0, range_bits))
        np.savez_compressed(dirname+"test_05_MYUV_realNoise" + "Y_CDS_" +"z" +str(0).zfill(3)+ "_f"+str(index).zfill(3) + ".npz", cam-cam0)
        print("saving: " + "Y_CDS_" +"z" +str(0).zfill(3)+ "_f"+str(index).zfill(3) + ".npz")
        cds = 0
    else:
        cam0= cam
        cds+=1