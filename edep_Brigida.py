#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 18:26:40 2021

@author: rahch
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import *
from scipy.stats import norm
import cv2

def pairs(Ekin, edep):
    int_data = np.loadtxt('/home/rahch/Workspace/Raspcam/DATA/Data_simu_Agosto_5_2021/plot-data.csv', skiprows=1, delimiter=',' )
    int_data_s = int_data[int_data[:,0].argsort()]
    x = int_data_s[:,0]
    y =  int_data_s[:,1]
    lin_inter = interp1d(x,y, kind='linear')
    
    Etotal = 1000000.0*(Ekin + 0.51099895000)
    if Etotal>10000:
        epsilon= 3.63
    else:
        epsilon = lin_inter(Etotal)
    mean_pair = edep*1000.0/epsilon
    #print(mean_pair)
    Fano = 0.117
    #print(mean_pair, edep*1000)
    return abs(int(np.floor(norm.rvs(loc=mean_pair, scale= np.sqrt(Fano*mean_pair),  size=1))))

dirname="/home/rahch/Workspace/Raspcam/Scripts/electron_2.6MeV_400um/"
filename0 = dirname + "E2600keV_e_edep_all_electron_arrived_detector.csv"
dirname2="/home/rahch/Workspace/Raspcam/Scripts/"
filename1 = dirname2 + "pairs_brigida_points.csv"
all_edep = np.loadtxt(filename0, skiprows=1, usecols=(0,1,2,3,4,6,7,8))
brigida_pairs = np.loadtxt(filename1,delimiter=",")
p_brig = brigida_pairs[:,1]
print(p_brig.size)
edep = np.float64(all_edep[:,0])
energy = all_edep[:,3]
pares = np.zeros(edep.size)
for i in range(edep.size):
    pares[i] = pairs(energy[i], edep[i])
    #print(pares[i])
    
hist, bin_edges = np.histogram(edep, bins=20000, density=False)
x = np.arange(1000)
scaled_hist  = hist/2.5
MC_label = "Geant4 " + r'$\beta \gamma = 5$' + " Edep"
plt.figure(figsize=(38.4,21.6))
plt.rcParams.update({'font.size': 25})
#plt.hist(x, weights=scaled_hist ,bins=1000, histtype='step', linewidth=2, label=MC_label)
plt.hist(edep ,bins=400, histtype='step', linewidth=2, label=MC_label)
plt.xlabel("Energy Loss (keV)")
plt.ylabel("number of events / 2.5 keV")
plt.title("Geant4 simulations 400um silicon's thikness")
plt.xlim(0,1000.0)
plt.legend()
plt.savefig("../Geant4_400um_edep.png")

plt.figure(figsize=(38.4,21.6))
plt.rcParams.update({'font.size': 25})
#hist0, bin_edges = np.histogram(pares/1000, bins=1000, density=False)
x = np.arange(0.0,100.0, 2.5)
#scaled_hist0 = hist0
#plt.hist(x, weights=scaled_hist0 ,bins=50, histtype='step', linewidth=2, label=MC_label)
plt.hist(pares/1000 ,bins=80,range=(0,200), histtype='step', linewidth=2, label=MC_label, density=True)
plt.hist(brigida_pairs[:,0], weights=brigida_pairs[:,1] ,bins=80,range=(0,200), density=True,histtype='step', linewidth=2, label="Brigida paper")
plt.xlabel("Number of pairs" +r'$\times 10^3  $')
plt.ylabel("number of events " )
plt.title("Geant4 simulations 400um silicon's thikness")
plt.xlim(0, 200)
plt.legend()
plt.savefig("../Geant4_400um_pares_norm.png", transparent=False)