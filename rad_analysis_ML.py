#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 20:33:22 2021

@author: Rodrigo Helaconde
Scipy >1.3
Numpy >1.13
"""

from radioactive_functions import *
import numpy as np
import matplotlib
#matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
import scipy
import math
from scipy.optimize import curve_fit
from scipy.special import factorial
from scipy.optimize import minimize
from scipy.stats import poisson
from scipy.stats import norm
import cv2
import peakutils
import sys
import os
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.utils import shuffle
from sklearn import  preprocessing
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier

factor = 250
    
# Image size
width = 2592
height = 1944
z_count = 10# number of Z (Z varies 2mm for the data of September 2019, 1 mm otherwise)
nfiles = 500#number of files per every Z
fnfiles = np.float64(nfiles)
test_df = np.loadtxt("/home/rahch/Workspace/Raspcam/Scripts/data_training.csv", delimiter=',', skiprows=1)
test_df = shuffle(test_df, random_state=0)
#training_df = np.loadtxt("/home/rahch/Workspace/Raspcam/Scripts/sample_small.csv", delimiter=',', skiprows=1)

#add_df = np.loadtxt("/home/rahch/Workspace/Raspcam/Scripts/data_fondo_1000_CDS_br50_iso800_15_dic_2020/data_frame_z000data_fondo_1000_CDS_br50_iso800_15_dic_2020.csv", delimiter=',', skiprows=1)  
#print(training_df[124])
#print(training_df.shape)

x = preprocessing.scale(test_df[:,:7])
y = test_df[:,7]

#print(y.shape)
#x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.45, random_state=42)
#gbc = GradientBoostingClassifier()
#gbc.fit(x, y)

#model_logr = LogisticRegression(solver='liblinear', random_state=0).fit(x, y)
#svclassifier = SVC(kernel='rbf', gamma='auto', probability=True )
#svclassifier.fit(x, y)

#month="December"
month="September"
if len(sys.argv) > 1:
    dirname = sys.argv[1]
    z_count = np.int(sys.argv[2])
    nfiles = np.int(sys.argv[3])
    month = sys.argv[4]
    pname= ""
else:
    #dirname = "data_RX_CDS_1s_2cm_15_11_19"
    #dirname = "data_RX_CDS_1s_28_10_19"
    #dirname = "data_gamma_CDS_1000"
    #dirname="data_10um_Emin_0.029_Emax_7.327keV_ADC_lin_CFA_Bayer_wb1_uintclip"
    #dirname = "data_co60_CDS_cente_1000"
    dirname = "data_beta_CDS_1000"
    #dirname = "data_fondo_1000_CDS_br50_iso800_15_dic_2020"
    #dirname = "data_fondo_CDS_1000"
    #dirname = "Data_Geant4_5um"
    #dirname = "Data_Geant4_10um"
    #dirname = "data_alfa_CDS_1000"
    #dirname = "data_RX_CDS_1s_27_11_19"
    #dirname = "data_Sr90_thickZ_3um_substrat"
if(month=="September"):
    pbgname = "Results/ResultsSep/"
    #pbgname = "Results/ResultsBlank/"
    #bgname = "data_fondo_CDS_1000"
    #bgname = "data_fondo_1000_CDS_br50_iso800_15_dic_2020"
    #bgname = "data_beta_CDS_1000"
    bgname = dirname
    pname = "../../DATA/Data_Septiembre_2019/" #parent directory
    #pname = "../../DATA/"
    #pname = "../../DATA/Data_Junio_2020/" #parent directory
elif(month=="October"):
    pbgname = "Results/ResultsOct/"
    bgname= dirname
    #Nombre directorio de la data
    pname = "../../DATA/Data_Octubre_2019/" #parent directory
elif(month=="November"):
    pbgname = "Results/ResultsNov/"
    bgname="data_RX_fondo_CDS_1s_2cm_18_11_19"
    pname = "../../DATA/Data_Noviembre_2019/"
elif(month=="December"):
    pbgname = "Results/ResultsDec/"
    bgname=dirname
    pname = "../../DATA/Data_Diciembre_2019/"
elif(month=="Simulation"):
    pbgname = "Results/ResultsBlank/"
    bgname = "data_fondo_CDS_1000"
    #pname = "../../DATA/Data_Junio_2020/" #parent directory
    pname = "../../DATA/data_Sr90/"
    MeanFrame = np.load(pbgname+bgname+"_Mean.npy")
    SigmaFrame = np.load(pbgname+bgname+"_sigma.npy")
    MaxFrame = np.load(pbgname+bgname+"_Max.npy")
#dirname = "data_fondo_CDS_cente_1000"
try:
    os.makedirs("../"+dirname+"_ML")
except FileExistsError:
    # directory already exists
    pass


#dframe = "data_frame_"  
MeanFrame = np.load(pbgname+bgname+"_Mean.npy")
SigmaFrame = np.load(pbgname+bgname+"_sigma.npy")
MaxFrame = np.load(pbgname+bgname+"_Max.npy")
z_dat = np.zeros(z_count, dtype=np.float64)
meanhits = np.zeros(z_count, dtype=np.float64)
sigmahits = np.zeros(z_count, dtype=np.float64)
allcluster_zmean = np.zeros(z_count, dtype=np.float64)
allclusterz_sigma = np.zeros(z_count, dtype=np.float64)
cluster_zmean = np.zeros(z_count, dtype=np.float64)
clusterz_sigma = np.zeros(z_count, dtype=np.float64)
clustermin_zmean = np.zeros(z_count, dtype=np.float64)
clustermin_zsigma = np.zeros(z_count, dtype=np.float64)
pixel_zmean = np.zeros(z_count, dtype=np.float64)
pixel_zsigma = np.zeros(z_count, dtype=np.float64)
#cluster_densityz = np.zeros(z_count, dtype=np.float64)
ADC_thresh = 1
#print("Z:"+"\t"+"AllClMean:"+"\t"+ "allClSigma:"+"\t"+"ClMean:"+"\t"+ "ClSigma:"+"\t" +"minCLMean:"+"\t"+"minCLSigma:"+"\t"+"PixelMean:"+ "\t" +"PixelSigma:")


for z in range(z_count):
    #print("procesing: "+ str(z)+"...")
    dframe= "../"+ dirname+"/data_frame_z"+str(z).zfill(3)+dirname+".csv"
    filenamef = "Y_CDS_z" +str(z).zfill(3)+ "_f" #filename format
    #filenamef = "Y_CDS_"+"Emin_0.029_Emax_7.327keV_" +"z" +str(z).zfill(3)+ "_f" #filename format
    hits_ts = np.zeros(nfiles, dtype=np.float64)
    hit_count =0 # #number of possible hits
    histos = np.zeros(1030, dtype=np.float64)
    ac_frame =np.zeros((height,width),dtype=np.uint16)
    cluster_counter = np.zeros(nfiles)
    pixel_counter = np.zeros(nfiles)
    mincluster_counter = np.zeros(nfiles)
    allcluster_counter = np.zeros(nfiles)
    cluster_sigma = np.zeros(nfiles)
    #cluster_multiplicity = np.empty(0, dtype=np.float64)
    SeedEz = np.empty(0,dtype=np.float64)
    SeedEzSP = np.empty(0,dtype=np.float64)
    SeedEzC4 = np.empty(0,dtype=np.float64)
    TotalEz = np.empty(0,dtype=np.float64)
    TotalEzm4 = np.empty(0,dtype=np.float64)
    SigmaEz = np.empty(0,dtype=np.float64)
    SigmaEzm4 = np.empty(0,dtype=np.float64)
    MeanEz= np.empty(0,dtype=np.float64)
    ClSizez= np.empty(0,dtype=np.float64)
    ClSizezm4= np.empty(0,dtype=np.float64)
    Shape_symz = np.empty(0,dtype=np.float64)
    cluster_densityz = np.empty(0, dtype=np.float64)
    signal_labelz = np.empty(0, dtype=np.int16)
    max_clustersize_tmp = 0
    for n in range(nfiles):
        #red_channel = np.zeros((height,width),dtype=np.uint8)
        #green_channel = np.zeros((height,width),dtype=np.uint8)
        tmpname = pname + dirname + "/" + filenamef + str(n).zfill(3) + ".npz"
        tmp_data = np.load(tmpname)
        data = np.float64(tmp_data.f.arr_0)
        print(tmpname)
        #threshold = MaxFrame
        threshold = abs(MeanFrame) +5.0*SigmaFrame #filter threshold
        tmp_frame = np.ones((height, width),dtype=np.float64)
        #ev_matrix = abs(data) > MaxFrame
        ev_matrix = abs(data) > threshold #filtering
        hit_count = np.sum(ev_matrix)
        data_f = np.trunc(abs(data)*ev_matrix)
        data_f8b = np.uint8(data_f)
        red_channel = data_f8b
        green_channel = data_f8b
        new_image = np.dstack([red_channel,green_channel, data_f8b])
        ac_frame += np.uint16(data_f)
        #plt.imsave("frame_beta_"+str(n)+".png", np.uint8(data_f), cmap ="gray")
        #2nd filtering cluster size =1 and 0 <adc <2
        #ret, thresh = cv2.threshold(data_f8b,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        #temp variables 
        tmp_x = np.zeros(7, dtype=np.float64)
        #looking for clusters
        ret, thresh = cv2.threshold(data_f8b,0,255,cv2.THRESH_BINARY)
        n_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(thresh)
        singlepixelnumber = np.count_nonzero(stats[:, cv2.CC_STAT_AREA]==1)
        less5clusternumber = np.count_nonzero(stats[:, cv2.CC_STAT_AREA]<5)
        clusterm4number = np.count_nonzero(stats[:, cv2.CC_STAT_AREA]>4)
        MeanE = np.zeros(n_labels -1, dtype=np.float64) #average energy of a given cluster in ADC units
        TotalE= np.zeros(n_labels -1, dtype=np.float64) #Total energy of a given cluster in ADC units
        TotalESq= np.zeros(n_labels -1, dtype=np.float64) #Total energy^2 of a given cluster in ADC units
        SigmaEcl= np.zeros(n_labels -1, dtype=np.float64) #Total energy^2 of a given cluster in ADC units
        SeedE=np.zeros(n_labels -1, dtype=np.float64) #Max  energy of a given cluster in ADC units
        ClusterSize=np.zeros(n_labels -1, dtype=np.float64) #Size of a Cluster
        SinglePixelE= np.zeros(singlepixelnumber, dtype=np.float64)
        Clusterm4SeedE = np.zeros(clusterm4number, dtype= np.float64)
        ClusterSizem4 = np.zeros(clusterm4number, dtype= np.float64)
        TotalEm4 = np.zeros(clusterm4number, dtype= np.float64)
        SigmaEclm4= np.zeros(n_labels -1, dtype=np.float64)
        ShapeSym = np.zeros(n_labels-1, dtype= np.float64)
        ShapeSymm4 = np.zeros(n_labels-1, dtype= np.float64)
        Cluster_Density = np.zeros(n_labels-1, dtype=np.float64)
        signal_label = np.zeros(n_labels-1 , dtype=np.int8)
        #Signal variables
        tmp_CLSIZE = np.empty(0, dtype=np.float64)
        tmp_MEAN = np.empty(0, dtype=np.float64)
        tmp_TOTAL = np.empty(0, dtype=np.float64)
        tmp_SEED = np.empty(0, dtype=np.float64)
        tmp_SIGMA =np.empty(0, dtype=np.float64)
        tmp_SHAPE = np.empty(0, dtype=np.float64)
        tmp_DENSITY = np.empty(0, dtype=np.float64)
        #print(n , n_labels)
        cluster_ctmp = 0
        tmp_pixel_counter = 0
        size_thresh = 4
        kp_counter = 0
        cl_m4ii =0
        filtered_clusters = 0
        single_filtered = 0
        ms4_filtered = 0
        
        for i in range(1, n_labels):
            
            limit_reached = False
            ClusterSize[i-1]=stats[i, cv2.CC_STAT_AREA]
            x = stats[i, cv2.CC_STAT_LEFT]
            y = stats[i, cv2.CC_STAT_TOP]
            w = stats[i, cv2.CC_STAT_WIDTH]
            h = stats[i, cv2.CC_STAT_HEIGHT]
            if (w >= h): #ubicar el punto medio del cuadrado que contiene al cluster
                side = w
                if (w%2==0):
                    side = w//2 +1 
                dif = w-h
                if (dif%2==0):
                    if( h%2!=0):
                        middle_h = np.int(np.rint(h/2))
                        middle_w = np.int(np.rint(side/2))
                    else:
                        middle_h = np.int( h/2 +1)
                        middle_w = np.int(np.rint(side/2))
                else:
                    middle_h = np.int(np.rint((h+dif)/2))
                    middle_w = np.int(np.rint(side/2))
            else:
                side = h
                dif = h -w 
            if (side%2==0):
                    side +=1 
            if (dif%2==0):
                    if( w%2!=0):
                        middle_w = np.int(np.rint(w/2))
                        middle_h = np.int(np.rint(side/2))
                    else:
                        middle_w = np.int(w/2 +1)
                        middle_h = np.int(np.rint(side/2))
            else:
                    middle_w = np.int(np.rint((h+dif)/2))
                    middle_h = np.int(np.rint(side/2))
            radious = np.int(np.rint(np.sqrt(2.0)*(side/2.0)))
            if stats[i, cv2.CC_STAT_AREA] == 1:
                ShapeSym[i-1] = -1.0
            else:
                l=8
                li=0
                sym_arr = np.zeros(l)
                #eje y positivo
                r_counter =0
                n_pix =0 #número de pixeles prendidos en una linea
                #eje y positivo
                if ((x + middle_w ) < width -1) and ((y+ middle_h)< height -1):
                    while (r_counter <= radious):
                        #print("1st pass")
                        iii = x + middle_w
                        jjj = y + middle_h +r_counter
                        new_image[jjj][iii][1]=255
                        #print (iii, jjj)
                        if (y + middle_h +r_counter ) < height -1 :
                            if data_f[jjj][iii] > 0  :
                                n_pix +=1 
                            r_counter+=1
                        else:
                            r_counter = radious +1
                
                    sym_arr[li]= np.power((n_pix - radious),2.0)
                    li+=1
                    r_counter =0
                    n_pix =0 #número de pixeles prendidos en una linea
                    #eje y negativo
                    while r_counter <= radious:
                        #print("2 pass")
                        new_image[y + middle_h -r_counter][x+middle_w][1]=255
                        if( data_f[y + middle_h -r_counter][x+middle_w] >0 ):
                            n_pix +=1 
                        r_counter+=1
                    sym_arr[li]= np.power((n_pix - radious),2.0)
                    li+=1
                    r_counter =0
                    n_pix =0 #número de pixeles prendidos en una linea
                    #eje x positivo
                    while r_counter <= radious:
                        #print("3 pass")
                        new_image[y + middle_h ][x+middle_w + r_counter][1]=255
                        if( data_f[y + middle_h ][x+middle_w + r_counter] >0 ):
                            n_pix +=1 
                        if (x+middle_w +r_counter ) < width -1 :
                            r_counter+=1
                        else:
                            #print("limit reached")
                            r_counter = radious +1
                            limit_reached = True
                            #print(np.power((n_pix - radious),2.0))
                            
                    sym_arr[li]= np.power((n_pix - radious),2.0)
                    li+=1
                    r_counter =0
                    n_pix =0 #número de pixeles prendidos en una linea
                    #eje x negativo
                    while r_counter <= radious:
                        #print("4 pass")
                        new_image[y + middle_h ][x+middle_w - r_counter][1]=255
                        if( data_f[y + middle_h ][x+middle_w - r_counter] >0 ):
                            n_pix +=1 
                        if (x+middle_w +r_counter ) < width -1 :
                                r_counter+=1
                        else:
                            #print("limit reached")
                            r_counter = radious+1
                            limit_reached = True
                            #print(np.power((n_pix - radious),2.0))      
                            
                    sym_arr[li]= np.power((n_pix - radious),2.0)
                    li+=1
                    r_counter =0
                    n_pix =0 #número de pixeles prendidos en una linea
                #diagonal primer cuadrante
                    while r_counter < radious:
                        if (y + middle_h +r_counter ) < height - 1:
                            #print(i, " 1st pass" )
                            if (x+middle_w +r_counter ) < width -1 :
                                #print(i, " 2nd pass" )
                                new_image[y + middle_h +r_counter ][x+middle_w +r_counter][1]=255
                                if data_f[y + middle_h +r_counter ][x+middle_w +r_counter] >0:
                                   # print(i, " 3rd pass" )
                                    n_pix +=1 
                                r_counter+=1
                            
                                
                            else:
                                r_counter = radious+1
                        else:
                            #print("limit reached")
                            r_counter = radious+1
                            limit_reached = True
                            #print(np.power((n_pix - radious),2.0))   
                            
                    sym_arr[li]= np.power((n_pix - radious),2.0)
                    li+=1
                    r_counter =0
                    n_pix =0 #número de pixeles prendidos en una linea
                    #diagonal 2do cuadrante
                    while r_counter < radious:
                        if (y + middle_h +r_counter ) < height - 1:
                            new_image[y + middle_h +r_counter ][x+middle_w -r_counter][1]=255
                            if( data_f[y + middle_h +r_counter ][x+middle_w -r_counter] >0 ):
                                n_pix +=1 
                            r_counter+=1
                        else:
                            r_counter = radious+1
                    sym_arr[li]= np.power((n_pix - radious),2.0)
                    li+=1
                    r_counter =0
                    n_pix =0 #número de pixeles prendidos en una linea
                #diagonal 3er cuadrante
                    while r_counter < radious:
                        new_image[y + middle_h - r_counter ][x+middle_w -r_counter][1]=255
                        if( data_f[y + middle_h - r_counter ][x+middle_w -r_counter] >0 ):
                            n_pix +=1 
                        r_counter+=1
                    sym_arr[li]= np.power((n_pix - radious),2.0)
                    li+=1
                    r_counter =0
                    n_pix =0 #número de pixeles prendidos en una linea
                #diagonal 4to cuadrante
                    while r_counter < radious:
                        new_image[y + middle_h - r_counter ][x+middle_w +r_counter][1] =255
                        if( data_f[y + middle_h - r_counter ][x+middle_w +r_counter] >0 ):
                            n_pix +=1 
                        if (x+middle_w +r_counter ) < width -1 :
                            r_counter+=1
                        else:
                            #print("limit reached")
                            r_counter = radious+1
                            limit_reached = True
                            #print(np.power((n_pix - radious),2.0))                        
                    sym_arr[li]= np.power((n_pix - radious),2.0)
                    
                    if(np.sum(sym_arr) ==0.0):
                        sym_arr[7] = -2.0
                    #ShapeSym[i-1] = (np.pi*radious*radious)*(l-1)/(np.sum(sym_arr)*ClusterSize[i-1]) 
                    ShapeSym[i-1] = (l-1)/(np.sum(sym_arr))
                    #if(limit_reached):   #debug
                        #print("shapesym: ",ShapeSym[i-1]) #debug
                    if(ShapeSym[i-1] < 0.0):
                        print("Shapesym Negative:", ShapeSym[i-1])
            if stats[i, cv2.CC_STAT_AREA] >= 1:
                if(stats[i, cv2.CC_STAT_AREA] <= size_thresh):
                    tmp_pixel_counter += 1
                    Cluster_Density[i-1]= -1.0
                    #aqui colocar densidad
                cluster_ctmp +=1
                #cluster_multiplicity[stats[i, cv2.CC_STAT_AREA]]+=1
                
                if(stats[i, cv2.CC_STAT_AREA] > max_clustersize_tmp):
                    max_clustersize_tmp = stats[i, cv2.CC_STAT_AREA]
                #print(i, stats[i, cv2.CC_STAT_AREA])
               
                #cv2.rectangle(new_image, (x, y), (x+w, y+h), (255, 0, 0), thickness=1)
                #cv2.circle(new_image,(x+middle_w, y+middle_h),radious,(255,0,0), thickness=1)
            tmp_Emax = np.float64(0.0)
            
            if (ClusterSize[i-1] >1.0):
                for jj in range(y, y+h):
                    for ii in range(x,x+w):
                        TotalE[i-1]+=data_f[jj][ii]
                        TotalESq[i-1]+=data_f[jj][ii]*data_f[jj][ii]#adding this to calculate the variance of a given cluster
                        if (tmp_Emax < data_f[jj][ii]):
                            tmp_Emax = data_f[jj][ii]
                Cluster_Density[i-1] = ClusterSize[i-1]/(np.pi*np.float64(radious*radious))
                SeedE[i-1]=tmp_Emax
                MeanE[i-1] = TotalE[i-1]/ClusterSize[i-1]
                SigmaEcl[i-1] = np.sqrt((TotalESq[i-1] - MeanE[i-1]*MeanE[i-1])/ClusterSize[i-1]) 
                if( MeanE[i-1] > 4.0 and SigmaEcl[i-1] > 3.0 ):
                    signal_label[i-1] = 1
            
            else:
                TotalE[i-1]= data_f[stats[i, cv2.CC_STAT_TOP]][stats[i, cv2.CC_STAT_LEFT]]
                SeedE[i-1]= data_f[stats[i, cv2.CC_STAT_TOP]][stats[i, cv2.CC_STAT_LEFT]]
                MeanE[i-1]= data_f[stats[i, cv2.CC_STAT_TOP]][stats[i, cv2.CC_STAT_LEFT]]
                SinglePixelE[kp_counter] = SeedE[i-1]
                SigmaEcl[i-1]= 0.0
                if TotalE[i-1] >= 4.0:
                    signal_label[i-1]= 1
                #kp_counter+=1
            
            tmp_x[0] = ClusterSize[i-1]
            tmp_x[1] = MeanE[i-1]
            tmp_x[2] = TotalE[i-1]
            tmp_x[3] = SeedE[i-1]
            tmp_x[4] = SigmaEcl[i-1]
            tmp_x[5] = ShapeSym[i-1]
            tmp_x[6] = Cluster_Density[i-1]
            #print(gbc.predict(tmp_x.reshape(1, -1)), signal_label[i-1], model_logr.predict(tmp_x.reshape(1, -1)))
            if(signal_label[i-1]):
                 #print(svclassifier.predict(tmp_x.reshape(1, -1)))
            #if(np.int(svclassifier.predict(tmp_x.reshape(1, -1)))):
                 tmp_CLSIZE = np.append(tmp_CLSIZE, tmp_x[0])
                 tmp_MEAN = np.append(tmp_MEAN, tmp_x[1])
                 tmp_TOTAL = np.append(tmp_TOTAL, tmp_x[2])
                 tmp_SEED = np.append(tmp_SEED, tmp_x[3])
                 tmp_SIGMA = np.append(tmp_SIGMA, tmp_x[4])
                 tmp_SHAPE = np.append(tmp_SHAPE, tmp_x[5])
                 tmp_DENSITY = np.append(tmp_DENSITY, tmp_x[6])
                 if(tmp_x[0]>4):
                    Clusterm4SeedE[cl_m4ii] = SeedE[i-1]
                    ClusterSizem4[cl_m4ii] = ClusterSize[i-1]
                    TotalEm4[cl_m4ii] = TotalE[i-1]
                    SigmaEclm4[cl_m4ii] = SigmaEcl[i-1]
                    cl_m4ii+=1
                 elif(ClusterSize[i-1]==1):
                    SinglePixelE[kp_counter] = tmp_x[3]
                    kp_counter+=1
            else:
                filtered_clusters+=1
                if tmp_x[0] == 1:
                    single_filtered +=1
                if tmp_x[0] <= size_thresh:
                     ms4_filtered += 1
        #end loop over clusters
        print("Filtered Clusters: ", filtered_clusters)
        print("Single Pixel: ", singlepixelnumber)
        #cv2.imwrite(dirname+"_z"+str(z)+"f"+str(n)+".jpg", new_image)
        cluster_counter[n]= cl_m4ii
        #print(cluster_counter[n], singlepixelnumber, less5clusternumber, n_labels )
        pixel_counter[n] = kp_counter
        mincluster_counter[n] =  n_labels - cl_m4ii - filtered_clusters
        allcluster_counter[n] = n_labels - filtered_clusters
        condition = Clusterm4SeedE>0
        condition1 = TotalEm4>0
        condition2 = ClusterSizem4>0
        SeedEz = np.append(SeedEz, tmp_SEED)
        TotalEz = np.append(TotalEz,tmp_TOTAL)
        SigmaEz = np.append(SigmaEz,tmp_SIGMA)
        TotalEzm4 = np.append(TotalEzm4,np.extract(condition1,TotalEm4))
        MeanEz = np.append(MeanEz, tmp_MEAN)
        SeedEzSP = np.append(SeedEzSP, SinglePixelE)
        SeedEzC4 = np.append(SeedEzC4, np.extract(condition, Clusterm4SeedE))
        ClSizez = np.append(ClSizez, tmp_CLSIZE)
        ClSizezm4 = np.append(ClSizezm4, np.extract(condition2,ClusterSizem4))
        Shape_symz =np.append(Shape_symz, tmp_SHAPE)
        cluster_densityz = np.append(cluster_densityz, tmp_DENSITY)
        signal_labelz = np.append(signal_labelz, signal_label)
        
        del ShapeSym
        #cluster_counter[n]= cluster_ctmp 
        #print(n, pixel_counter, cluster_counter[n], n_labels)
        
    np.savetxt(dframe, np.vstack((ClSizez, MeanEz, TotalEz, SeedEz, SigmaEz, Shape_symz, cluster_densityz)).T, delimiter="," , newline="\n",fmt="%12.6f" , header="Clsize, Mean, Total, Seed, Std-dev, Shape_parameter, Density")
    z_dat[z] = np.longdouble(z)*2.0
    meanhits[z] = np.average(hits_ts)
    cmean, csigma = plot_z2(cluster_counter,z,dirname,"minsize5")
    c4mean, c4sigma = plot_z2(mincluster_counter,z,dirname,"minsize1-4")
    pmean, psigma = plot_z2(pixel_counter,z,dirname,"1pixelsize")
    allcmean, allcsigma = plot_z2(allcluster_counter,z,dirname,"allclusters")
    plotMultiplicityHisto(ClSizez,z, dirname,max_clustersize_tmp )
    PlotSeedHisto(SeedEz, z, dirname,'allclusters')
    PlotClMeanHisto(MeanEz, z, dirname,'allclusters')
    PlotClTotalEHisto(TotalEz,z,dirname, 'allclusters', False)
    PlotClTotalEHisto(TotalEz,z,dirname, 'allclusters',True)
    #print(TotalEzm4)
    PlotShapeHisto(Shape_symz,z,dirname, 'allclusters', False)
    PlotSigmaHisto(SigmaEz,z,dirname, 'allclusters', False)
    PlotClTotalEHisto(TotalEzm4,z,dirname, 'cluster_ms_5', False)
    PlotClTotalEHisto(TotalEzm4,z,dirname, 'cluster_ms_5',True)
    PlotSeedHisto(SeedEzSP, z, dirname,'SinglePixelClusters')
    PlotSeedHisto(SeedEzC4, z, dirname,'Clusters_min_size5')
    PlotSeedClusterSize(SeedEz, ClSizez, z, dirname, "Seedsignal", "ClusterSize", "all" )
    PlotSeedClusterSize(TotalEzm4, ClSizezm4, z, dirname, "TotalE", "ClusterSize","minsize5" )
    PlotDensHisto(cluster_densityz,z,dirname, 'allclusters', False)
    #PlotScatterBubble(SeedEz, ClSizez, z, dirname, "Seedsignal vs ClusterSize" )
    del SeedEz
    del MeanEz
    del TotalEz
    del TotalEzm4
    del SeedEzSP
    del SeedEzC4
    del ClSizezm4
    del SigmaEz
    del Shape_symz
    #cluster_zmean[z] = np.average(cluster_counter)
    #clusterz_sigma[z] = np.std(cluster_counter,ddof=1)
    cluster_zmean[z] = cmean
    clusterz_sigma[z] = csigma
    allcluster_zmean[z] = allcmean
    allclusterz_sigma[z] = allcsigma
    clustermin_zmean[z] = c4mean
    clustermin_zsigma[z] = c4sigma
    pixel_zmean[z] = pmean
    pixel_zsigma[z] = psigma
    #.print(z , allcmean, allcsigma, cmean, csigma ,c4mean, c4sigma, pmean, psigma )
    mean_tmp = meanhits[z]
    sigmahits[z]= np.std(hits_ts,ddof=1)
    
if(z_count > 3):
    plot_nz(cluster_zmean,clusterz_sigma,z_count,dirname, "minsize5")
    plot_nz(clustermin_zmean,clustermin_zsigma,z_count,dirname, "maxsize4")
    plot_nz(pixel_zmean,pixel_zsigma,z_count,dirname, "SinglePixel")
    plot_nz(allcluster_zmean,allclusterz_sigma,z_count,dirname, "allclusters")
