#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 10:22:12 2020

@author: rahch
"""
#import matplotlib
#matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.stats import norm
import scipy.stats
#plt.figure(figsize=(38.4,21.6))
plt.rcParams.update({'font.size': 25})


fig, ax = plt.subplots(1,figsize=(38.4,21.6),sharey=True )

#data_alpha = np.load("../data_alfa_CDS_1000/ClMultiplicitydata_alfa_CDS_1000z_0.npy")
data_beta = np.load("../data_beta_CDS_1000/3.0Sigma - ADC 1/ClMultiplicitydata_beta_CDS_1000z_0.npy")
data_beta2 = np.load("../data_beta_CDS_1000/ClMultiplicitydata_beta_CDS_1000z_0.npy")
data_beta1 = np.load("../data_beta_CDS_1000/3sigma/ClMultiplicitydata_beta_CDS_1000z_0.npy")
#data_beta = np.load("../Sr90-ADC1/ClMultiplicitydata_beta_CDS_1000z_0.npy")
#data_gamma = np.load("../data_gamma_CDS_1000/ClMultiplicitydata_gamma_CDS_1000z_0.npy")
data_sim3um = np.load("../data_Sr90_thickZ_3um_substrat/ClMultiplicitydata_Sr90_thickZ_3um_substratz_0.npy")
data_sim5um= np.load("../data_Sr90_thickZ_5um_substrat/ClMultiplicitydata_Sr90_thickZ_5um_substratz_0.npy")
data_sim7um = np.load("../data_Sr90_thickZ_7um_substrat/ClMultiplicitydata_Sr90_thickZ_7um_substratz_0.npy")
data_sim10um= np.load("../data_Sr90_thickZ_10um_substrat/ClMultiplicitydata_Sr90_thickZ_10um_substratz_0.npy")
#msr90 = np.mean(data_beta)
#sgsr90 = np.std(data_beta, ddof=1)
size3um= data_sim3um.size
sizesr90 = data_beta.size
sizesr902 = data_beta2.size
sizesr901 = data_beta1.size
size5um = data_sim5um.size
size7um = data_sim7um.size 
size10um = data_sim10um.size 
i = sizesr90 -1
j = size3um -1
k = size5um -1
l =  size7um -1
m =  size10um -1

while(data_beta[i] == 0.0):
    i-=1
    #print(i, data_beta[i])

maxcssr90 = i

while(data_sim3um[j]== 0.0):
    j-=1
    #print()
maxcscs137 = j

while(data_sim5um[k]== 0.0):
    k-=1
    #print()
maxcsam241 = k

while( data_sim7um[l]== 0.0):
    l-=1
    #print()
maxcsimsr90 = l
while( data_sim10um[m]== 0.0):
    m-=1
    #print()
maxcsims10um = m

maxcs = np.max(np.array((i,j,k,l,m)))


"""
iqr= scipy.stats.iqr(data_beta)
FDR= 2*iqr/np.cbrt(data_beta.size)
if FDR !=0.0:
    nbins=np.int(np.ceil(np.max(data_beta)/FDR))
else:
    h=3.5*sgsr90/np.cbrt(data_beta.size)
    if (h > 0.0):
            nbins=np.int(np.ceil(np.max(data_beta)/h))
#print(msr90,sgsr90, nbins, data_beta.size)
"""
norm = True
entries0, bin_edges0, patches0 = ax.hist(np.arange(sizesr90), bins=sizesr90, weights=data_beta, density=norm, log=True, histtype='step', label="Sr90 ADC Threshold 1",color='C0', linewidth=2)
#entries4, bin_edges4, patches4 = ax[1].hist(np.arange(sizesr90), bins=sizesr90, weights=data_beta, density=True, log=True, histtype='step', label="Sr90",color='C0')
#entries1, bin_edges1, patches1 = ax.hist(np.arange(size3um), bins=size3um, weights=data_sim3um, density=True, log=True, histtype='step', label="3um subs", color='C2', linewidth=2)
#entries2, bin_edges2, patches2 = ax.hist(np.arange(size5um), bins=size5um, weights=data_sim5um, density=True, log=True, histtype='step', label=" 5um subs",color='C4', linewidth=2)
#entries5, bin_edges5, patches5 = ax.hist(np.arange(size7um), bins=size7um, weights=data_sim7um, density=True, log=True, histtype='step', label="7um subs",color='C1', linewidth=2)
entries3, bin_edges3, patches3 = ax.hist(np.arange(size10um), bins=size10um, weights=data_sim10um, density=norm, log=True, histtype='step', label="10um subs", color='C3', linewidth=2)
entries6, bin_edges6, patches6 = ax.hist(np.arange(sizesr902), bins=sizesr902, weights=data_beta2, density=norm, log=True, histtype='step', label="Sr90 ADC Threshold 2",color='C6', linewidth=2)
entries7, bin_edges7, patches7 = ax.hist(np.arange(sizesr901), bins=sizesr901, weights=data_beta1, density=norm, log=True, histtype='step', label="Sr90 ADC Threshold 0",color='C7', linewidth=2)

fig.suptitle('Cluster Multiplicity Z= 0mm')
#for i in range(2):
ax.set_xlim(0,int(maxcs))
ax.set_xlabel('Cluster Size')
ax.set_ylabel('Frecuency')
ax.legend()
ax.grid(True)

plt.savefig("../plots/t02_norm.png")
plt.show()
