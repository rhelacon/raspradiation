#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 13 23:52:13 2021

@author: rahch
"""

from radioactive_functions import *
import numpy as np
import matplotlib
#matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
import scipy
import math
from scipy.optimize import curve_fit
from scipy.special import factorial
from scipy.optimize import minimize
from scipy.stats import poisson
from scipy.stats import norm
import cv2
import peakutils
import sys
import os

factor = 250
    
# Image size
width = 2592
height = 1944
z_count = 1# number of Z (Z varies 2mm for the data of September 2019, 1 mm otherwise)
nfiles = 1000#number of files per every Z
bits10 = False
fnfiles = np.float64(nfiles)
old_frame = np.zeros((height,width), dtype=np.float64)


pname = "../../DATA/Data_Septiembre_2021/"
dirname = "data_Sr90_frame1001_iso0_br50_ag8_ad1_2021_Sep_06_15h_10m_43s"
result_dir = pname + "data_beta_CDS_ag8/"
try:
    os.makedirs(result_dir)
except FileExistsError:
    # directory already exists
    pass
CDS_boolean = False
for i in range(nfiles):
    test = i- 1
    filenamef = "RGB_" +"z" +str(0).zfill(3)+ "_f"
    tmpname = pname + dirname + "/" + filenamef + str(i).zfill(3) + ".npz"
    
    print(tmpname)
    if CDS_boolean:
        tmp_data = np.load(tmpname)
        frame = np.float64(tmp_data.f.arr_0)
        CDS = frame - old_frame
        CDS_boolean = False
        np.savez_compressed(result_dir + "Y_CDS_" +"z" +str(0).zfill(3)+ "_f"+str(int(test/2)).zfill(3) + ".npz", CDS)
        print("saving CDS: ", test, i, int(test/2))
        
    else:
        tmp_data = np.load(tmpname)
        old_frame = np.float64(tmp_data.f.arr_0)
        CDS_boolean = True
        
        
        