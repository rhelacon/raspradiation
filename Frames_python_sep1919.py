#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
# Image size
width = 2592
height = 1944

filenamef = "Y_CDS_z000_f" #filename format
pname = "../DATA/Data_Noviembre_2019/" #parent directory
dirname = "data_RX_fondo_CDS_1s_2cm_18_11_19" #Nombre directorio de la data
z_count = 1 # number of Z (Z varies 2mm for the data of September 2019, 1 mm otherwise)
hit_count =0 # #number of possible hits
nfiles = 10000 #number of files
fnfiles = np.float64(nfiles)
MeanFrame = np.zeros((height, width),dtype=np.float64)
SigmaFrame = np.zeros((height, width),dtype=np.float64)
SumFrame = np.zeros((height, width),dtype=np.float64)
Sum2Frame = np.zeros((height, width),dtype=np.float64)
#OldFrame = np.zeros((height, width),dtype=np.float64)
MaxFrame = np.zeros((height, width),dtype=np.float64)

for n in range(nfiles):
    tmpname = pname + dirname + "/" + filenamef + str(n).zfill(3) + ".npz"
    tmp_nfiles = nfiles
    tmp_fnfiles = np.float64(nfiles)
    try:
        tmp_data = np.load(tmpname)
        ReadOk= True     
    except: 
        print("error on: " + tmpname)
        ReadOk= False
        tmp_nfiles= tmp_nfiles -1
        tmp_fnfiles= np.float64(tmp_nfiles)
    if(ReadOk):        
        data = np.float64(tmp_data.f.arr_0)
        SumFrame += data
        Sum2Frame += data**2
        print(tmpname)

        bo = abs(data)>=MaxFrame
        b1= abs(data)<MaxFrame
        MaxFrame = bo*abs(data) + b1*MaxFrame   
    #print(data.shape)    
 #   for i in range(height):
 #       for j in range(width):
 #           if(abs(data[i][j]) > MaxFrame[i][j]):
 #               MaxFrame[i][j]= abs(data[i][j])                
           

    
MeanFrame = SumFrame/tmp_nfiles
SigmaFrame = (Sum2Frame - tmp_fnfiles*MeanFrame**2)/(tmp_fnfiles - 1.0)
SigmaFrame = np.sqrt(SigmaFrame)
np.save("ResultsNov/"+dirname+"_sigma", SigmaFrame)
np.save("ResultsNov/"+dirname+"_Mean", MeanFrame)
np.save("ResultsNov/"+dirname+"_Max", MaxFrame)
