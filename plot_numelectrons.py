#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 18 20:17:28 2021

@author: rahch
"""

import matplotlib
#matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.stats import norm
from scipy import stats
from scipy.interpolate import CubicSpline
from scipy.stats import chi2
plt.figure(figsize=(38.4,21.6))
plt.rcParams.update({'font.size': 25})

def chi2_sigma(o,e, sigma):
    chi=np.float_power(o -e, 2.0)/np.power(sigma,2.0)
    return np.sum(chi)

parent_dir = "/home/rahch/Workspace/Raspcam/Scripts/num_electron_Septiembre_2021/Numb_electron_Sr90/"
#filename = "data_beta_CDS_1000NCl_vs_Zallclusters_expdecay.npy"
"""listdir = ["Numb_elect_data_Sr90_pixl_thickZ_10um_11level/number_electron_track_event_allframe_cds_10um.npy",
           "data_Sr90_frame100_iso0_br50_ag8_ad1_2021_Sep_24_CDS_5.0sigma/data_Sr90_frame100_iso0_br50_ag8_ad1_2021_Sep_24_CDS_5.0sigmaNCl_vs_Zallclusters_expdecay.npy",
           "data_Sr90_frame100_iso0_br50_ag8_ad1_2021_Sep_24_CDS_6.0sigma/data_Sr90_frame100_iso0_br50_ag8_ad1_2021_Sep_24_CDS_6.0sigmaNCl_vs_Zallclusters_expdecay.npy",
           "data_Sr90_frame100_iso0_br50_ag8_ad1_2021_Sep_24_CDS_7.0sigma/data_Sr90_frame100_iso0_br50_ag8_ad1_2021_Sep_24_CDS_7.0sigmaNCl_vs_Zallclusters_expdecay.npy",
           "data_Sr90_frame100_iso0_br50_ag8_ad1_2021_Sep_24_CDS_9.0sigma/data_Sr90_frame100_iso0_br50_ag8_ad1_2021_Sep_24_CDS_9.0sigmaNCl_vs_Zallclusters_expdecay.npy"]
"""


listdir = ["Numb_elect_data_Sr90_pixl_thickZ_10um_11level/number_electron_track_event_allframe_cds_10um.npy",
           "data_Sr90_thickZ_10um_elect_hole_pairs_3.6eV_ag8_noise_real_CDS_5.0sigma/data_Sr90_thickZ_10um_elect_hole_pairs_3.6eV_ag8_noise_real_CDS_5.0sigmaNCl_vs_Zallclusters_expdecay.npy",
           "data_Sr90_thickZ_10um_elect_hole_pairs_3.6eV_ag8_noise_real_CDS_6.0sigma/data_Sr90_thickZ_10um_elect_hole_pairs_3.6eV_ag8_noise_real_CDS_6.0sigmaNCl_vs_Zallclusters_expdecay.npy",
           "data_Sr90_thickZ_10um_elect_hole_pairs_3.6eV_ag8_noise_real_CDS_7.0sigma/data_Sr90_thickZ_10um_elect_hole_pairs_3.6eV_ag8_noise_real_CDS_7.0sigmaNCl_vs_Zallclusters_expdecay.npy",
           "data_Sr90_thickZ_10um_elect_hole_pairs_3.6eV_ag8_noise_real_CDS_9.0sigma/data_Sr90_thickZ_10um_elect_hole_pairs_3.6eV_ag8_noise_real_CDS_9.0sigmaNCl_vs_Zallclusters_expdecay.npy"]

list_fmt = ['o', 'v' ,'^','d','<' ,'s' ]
list_color= ['C0', 'C1', 'C2','C3','C4', 'C5' ]
#list_legend = ['MC', '3.0 sigma','3.5 sigma','4.0 sigma','4.5 sigma'  ,'5.0 sigma' ]
list_legend = ['MC', 'MC 5.0 sigma','MC 6.0 sigma','MC 7.0 sigma','MC 9.0 sigma'  ,'11' ]
k = 0
x = 2.0*np.arange(11, dtype=float)
y_e = np.zeros(11)
for ldir in listdir:
    points = np.load(parent_dir+ldir)
    y = points[:,1]
    y_err = points[:,2]
    if k == 0:
        CS = CubicSpline(x,y)
        xs = np.arange(0.0,20.0, 0.01)
        plt.plot(xs ,CS(xs) , '-', color=list_color[k],linewidth=3 )
        plt.plot(x ,y , list_fmt[k], color=list_color[k], label=list_legend[k])
        y_e= y
    else:
        ji2 = chi2_sigma(y, y_e, y_err)
        prob = chi2.sf(ji2,9)
        ji_str = r'$ \chi^2$'+": " + '%.3f'%ji2 +'\n'+'prob: '+ '%.3f'%prob 
        plt.errorbar(x, y, yerr=y_err, color=list_color[k], fmt=list_fmt[k],label=list_legend[k]+"\n"+ji_str, capsize=10.0, linewidth=3, ms=20.0)
        #plt.plot(x, y,  list_fmt[k],ms=20.0, color=list_color[k],label=list_legend[k]+"\n"+ji_str,  linewidth=3)
    k+=1
plt.legend()
plt.xlim(-2.0,22.0)
plt.xlabel("Z(mm)")
plt.ylabel("Number of electrons")
plt.title("Z vs Number of electrons all clusters" )
plt.grid()
plt.savefig("../"+ "Z vs Number of electrons"+" MC_ALL_5sig_a_9sig_errbar"+"_allClusters" +".png")
plt.show()